//Подключаем dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let server = chai.request('http://localhost:10080');
let should = chai.should();
let startPath = '/auth';
chai.use(chaiHttp);
//Наш основной блок
describe('Auth', () => {
  let accessToken = '';
  let refreshToken = 'empty';
  beforeEach((done) => { //Перед каждым тестом получаем новый token
    let userTest = {
      username: 'newDebugUser',
      password: 'testUser2',
      grant_type: 'password',
      client_id: 'null',
      client_secret: 'null'
    };
    server
      .post(startPath + '/login')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(userTest)
      .end((err, res) => {
        should.exist(res.body);
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('access_token');
        res.body.should.have.property('refresh_token');
        accessToken = res.body.access_token;
        refreshToken = res.body.refresh_token;
        done();
      });
  });
  /*
    * Тест для /GET
    */
  describe('/POST register', () => {
    let userTest = {
      username: 'newDebugUser',
      password: 'testUser2'
    };
    it('register new user', (done) => {
      server
        .post(startPath + '/register')
        .send(userTest)
        .end((err, res) => {
          should.exist(res.body);
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');//.eql('Registration was successful');
          done();
        });
    });
    it('register old user', (done) => {
      server
        .post(startPath + '/register')
        .send(userTest)
        .end((err, res) => {
          should.exist(res.body);
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('User already exists');
          done();
        });
    });
  });

  describe('/POST login', () => {
    it('login new user', (done) => {
      let userTest = {
        username: 'newDebugUser',
        password: 'testUser2',
        grant_type: 'password',
        client_id: 'null',
        client_secret: 'null'
      };
      server
        .post(startPath + '/login')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send(userTest)
        .end((err, res) => {
          should.exist(res.body);
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('access_token');
          res.body.should.have.property('refresh_token');
          done();
        });
    });
  });

  describe('/POST refresh', () => {
    it('login new user on refresh token', (done) => {
      let userTest = {
        username: 'newDebugUser',
        refresh_token: refreshToken,
        grant_type: 'refresh_token',
        client_id: 'null',
        client_secret: 'null'
      };
      server
        .post(startPath + '/login')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send(userTest)
        .end((err, res) => {
          should.exist(res.body);
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('access_token');
          res.body.should.have.property('refresh_token');
          done();
        });
    });
  });

  describe('/GET isAuth', () => {
    it('check on auth', (done) => {
      isAuth(done);
    });
  });

  function isAuth (done2) {
    server
      .get(startPath + '/isAuth')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + accessToken)
      .end((err, res) => {
        should.exist(res.body);
        res.should.have.status(200);
        done2();
      });
  }

  function sendCode (done2) {
    let userTest = {
      number: '+9312624718'
    };
    server
      .post(startPath + '/sendCode')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(userTest)
      .end((err, res) => {
        should.exist(res.body);
        res.should.have.status(200);
        res.body.should.have.property('message').eql('Code send success');
        done2();
      });
  }

  function verifyCode (done2) {
    let data = {
      number: '+9312624718',
      code: 1234
    };
    server
      .post(startPath + '/verifyCode')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(data)
      .end((err, res) => {
        should.exist(res.body);
        res.should.have.status(200);
        res.body.should.have.property('access_token');
        res.body.should.have.property('refresh_token');
        done2();
        accessToken = res.body.refresh_token;
      });
  }

  describe('/POST send and verify code', () => {
    it('send code', (done) => {
      sendCode(done);
    });
    it('verify code', (done) => {
      verifyCode(done);
    });
    it('check what we really login', (done) => {
      isAuth(done);
    });

  });
});