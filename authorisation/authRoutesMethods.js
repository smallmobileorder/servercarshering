// const Nexmo = require('nexmo');
// const request = require('request');

let userDBHelper;
let app;
// import {delete_person_with_phone, delete_person} from '../dataquery/queryRoutesMethods';
const queryRM = require('../dataquery/queryRoutesMethods');
// const nexmo = new Nexmo({
//     apiKey: '18d2ed29',
//     apiSecret: 'IjrLlATd0bLFLNTe',
// });

module.exports = (injectedUserDBHelper, expressApp) => {
    userDBHelper = injectedUserDBHelper;

    app = expressApp;
    return {
        registerUser: registerUser,
        login: login,
        refreshToken: refreshToken,
        sendCode: sendCode,
        verifyCode: verifyCode,
        isAuth: isAuth,
    };
};


/* handles the api call to register the user and insert them into the users table.
  The req body should contain a username and password. */
function registerUser(req, res) {
    //query db to see if the user exists already
    userDBHelper.doesUserExist(req.body.username, (sqlError, doesUserExist) => {

        //check if the user exists
        if (sqlError !== null || doesUserExist) {

            //message to give summary to client
            const message = sqlError !== null ? sqlError : 'User already exists';

            sendResponse(res, message, sqlError);

            return;
        }
        //register the user in the db
        userDBHelper.registerUserInDB(req.body.username, req.body.password, (error) => {
            //create message for the api response
            const message = error === null ? 'Registration was successful' : 'Failed to register user';

            sendResponse(res, message, error);
        });
    });
}

/* handles the api call to register the user and insert them into the users table.
  The req body should contain a username and password. */
function sendCode(req, res) {
    //query db to see if the user exists already
    if (req.body.number !== null) {
        // nexmo.verify.request({
        //     number: req.body.number,
        //     brand: 'Nexmo',
        //     code_length: '4'
        // }, (err, result) => {
        //     console.log(err ? err : result);
        //     if (!err) {
        userDBHelper.insertPhoneRequest([req.body.number, 'qwertyuiop'], (error) => {
            //         userDBHelper.insertPhoneRequest([req.body.number, result.request_id], dataResponseObject => {
            //create message for the api response
            const message = error === null ? 'Code send success' : 'Failed to send_user';
            sendResponse(res, message, error);
        });
        // } else {
        //     sendResponse(res, null, err);
        // }
        // });
    } else {
        sendResponse(res, null, 'phone number can`t find in body');
    }
}

function verifyCode(req, res, next) {
    //query db to see if the user exists already
    if (req.body.code !== null || req.body.number !== null) {
        userDBHelper.getRequestId(req.body, (error, requestId) => {
                if (error !== null || requestId === null) {
                    sendResponse(res, 'can\'t find correct request id for this number' , error);
                } else {
                    // nexmo.verify.check({
                    //     request_id: requestId,
                    //     code: req.body.code
                    // }, (err, result) => {

                    // const error = (err !== null || result.status !== '0') ? err.toString() : null;
                    const error = (req.body.code !== '1234') ? 'it is fake response, try 1234 code' : null;
                    const message = error !== null ? null : 'number very success';
                    if (error === null) {
                        const timestamp = new Date(Date.now()).toLocaleString();
                        userDBHelper.registerUserInDB(req.body.number, timestamp, (error, results) => {
                            //create message for the api response
                            const message = error === null ? 'Registration was successful' : 'Failed to register user';

                            if (error === null) {
                                const check = app.oauth.grant();
                                const data = {
                                    username: req.body.number,
                                    password: timestamp,
                                    grant_type: 'password',
                                    client_id: 'null',
                                    client_secret: 'null'
                                };
                                req.body.id_user = results.rows[0].id;
                                req.body.first_name = 'empty';
                                req.body.last_name = 'empty';
                                req.body.phone_number = req.body.number;
                                insertFullClients(req, (results, error) => {
                                    if (error !== null) {
                                        sendResponse2(res, results, error);
                                    } else {
                                        console.log('req77777:', req.body);
                                        check(new reqForOAuth(data), res, next);
                                    }
                                });
                            } else {
                                sendResponse2(res, message, error);
                            }
                        });
                    } else {
                        sendResponse2(res, message, error);
                    }
                    // }
                    // );
                }
            }
        );
    } else {
        sendResponse2(res, null, 'code or/and number can`t find in body');
    }
}

function reqForOAuth(body) {
    this.method = 'POST';
    this.headers = {
        'content-type': 'application/x-www-form-urlencoded',  // <--Very important!!!
    };
    this.body = body;
    this.is = function (data) {
        return data === 'application/x-www-form-urlencoded';
    };
}

function insertFullClients(req, callback) {
    console.log('req1:', req.body);

    userDBHelper.insertPerson(req.body, (error, results) => {
        if (error === null) {
            req.body.id_person = results[0].id;
            req.body.id_user = req.user_id;
            console.log('req2:', req.body);
            userDBHelper.insertPersonWithPhone(req.body, (error, results) => {
                if (error === null) {
                    req.body.id_person_with_phone = results[0].id;
                    console.log('req3:', req.body);
                    userDBHelper.insertClient(req.body, (error, results) => {
                        if (error === null) {
                            console.log('req:', req.body);
                            callback(results[0], null);
                        } else {
                            callback(null, error);
                        }
                    });
                } else {
                    callback(null, error);
                }
            });
        } else {
            if (error.code !== 23505) {
                callback('User is exists', null);
            } else {
                callback(null, error);
            }
        }
    });
}


/* handles the api call to register the user and insert them into the users table.
  The req body should contain a username and password. */
function refreshToken(req, res) {
    sendResponse(res, 'test', null);
}


function login(registerUserQuery, res) {
    sendResponse(res, 'test', null);
}

function isAuth(req, res) {
    sendResponse(res, 'AllOk', null);
}


//sends a response created out of the specified parameters to the client.
//The typeOfCall is the purpose of the client's api call
function sendResponse2(res, message, error) {
    console.log(error === null);
    res
        .status(error !== null ? 400 : 200)
        .json(error !== null ? error : message);
}

function sendResponse(res, message, error) {

    res
        .status(error === null ? 200 : 400)
        .json({
            'message': message,
            'error': error,
        });
}
