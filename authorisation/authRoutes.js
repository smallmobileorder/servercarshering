module.exports = (router, expressApp, authRoutesMethods) => {

    //route for registering new users
    router.post('/register', authRoutesMethods.registerUser);

    router.post('/sendCode', authRoutesMethods.sendCode);

    router.post('/verifyCode', authRoutesMethods.verifyCode);

    //route for allowing existing users to login
    router.post('/login', expressApp.oauth.grant(), authRoutesMethods.login);

    //route for allowing existing users to login
    router.post('/refresh', expressApp.oauth.grant(), authRoutesMethods.refreshToken);

    router.all('/isAuth', expressApp.oauth.authorise(), authRoutesMethods.isAuth);

    return router;
};


