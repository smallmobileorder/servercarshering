let userDBHelper;
module.exports = injectedUserDBHelper => {

    userDBHelper = injectedUserDBHelper;

    return {
        infoAboutUpload: infoAboutUpload,
        accessUpload: accessUpload
    };
};

function infoAboutUpload(req, res) {
    console.log(req.headers);
    userDBHelper.insertFile([req.user.user_id, req.headers['origin-file'], req.headers['x-file']], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function accessUpload(req, res) {
    res.setHeader('qqqqq', 'wwwwwww')
        .status(200)
        .json({
            'message': 'good',
            'error': 'error'
        });
}

function sendResponse(res, message, error) {
    res
        .status(error === null ? 200 : 400)
        .json(error !== null ? error : message);
}