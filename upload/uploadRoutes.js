module.exports = (router, expressApp, restrictedAreaRoutesMethods) => {

    //route for entering into the restricted area.
    router.post('/', expressApp.oauth.authorise(), restrictedAreaRoutesMethods.infoAboutUpload);

    //route for entering into the restricted area.
    router.all('/access', expressApp.oauth.authorise(), restrictedAreaRoutesMethods.accessUpload);

    return router;
};
