let userDBHelper;
const googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyBxRP2KAzhATmAFkNTzjqgzP1tmQlPtc8M'
});
module.exports = injectedUserDBHelper => {

    userDBHelper = injectedUserDBHelper;

    return {
        getOrder: getOrder,
        getIssue: getIssue,
        getDriver: getDriver,
        getClient: getClient,
        getBlackList: getBlackList,
        getFreeOrders: getFreeOrders,
        getListIssues: getListIssues,
        getDirections: getDirections,
        getTransaction: getTransaction,
        getListDrivers: getListDrivers,
        getListClients: getListClients,
        getMainConfigs: getMainConfigs,
        getOrdersByState: getOrdersByState,
        getDriversTariffs: getDriversTariffs,
        getMainConfigById: getMainConfigById,
        getLastOrderClient: getLastOrderClient,
        getRouteOrderById: getRouteOrderById,
        getLastOrderDriver: getLastOrderDriver,
        getRequestForDriver: getRequestForDriver,
        getRequestForDrivers: getRequestForDrivers,
        getPositionByIdClient: getPositionByIdClient,
        getPositionByIdDriver: getPositionByIdDriver,
        getPositionsAllDriver: getPositionsAllDriver,
        getDriverTransactions: getDriverTransactions,
        getHistoryPositionsByIdDriver: getHistoryPositionsByIdDriver,
        getDriverOrders: getDriverOrders,
        getClientOrders: getClientOrders,
        postOrder: postOrder,
        insertCars: insertCars,
        insertPerson: insertPerson,
        insertDriver: insertDriver,
        insertComment: insertComment,
        insertLicense: insertLicense,
        insertPassport: insertPassport,
        insertMarkCars: insertMarkCars,
        insertModelCars: insertModelCars,
        insertBlackList: insertBlackList,
        insertIssueUser: insertIssueUser,
        insertMainConfig: insertMainConfig,
        insertTransaction: insertTransaction,
        insertDriverTariff: insertDriverTariff,
        insertPersonWithPhone: insertPersonWithPhone,
        insertRequestForDrivers: insertRequestForDrivers,
        insertFullRequestForDrivers: insertFullRequestForDrivers,
        updateOrder: updateOrder,
        updateIssueAdmin: updateIssueAdmin,
        updateMainConfig: updateMainConfig,
        updateOrderDriver: updateOrderDriver,
        updatePassport: updatePassport,
        updateLicense: updateLicense,
        updateCar: updateCar,
        updatePersonDriver: updatePersonDriver,
        updatePhoneEmailDriver: updatePhoneEmailDriver,
        updateRequestForDrivers: updateRequestForDrivers,
        updatePositionByIdDriver: updatePositionByIdDriver,
        updateTariffSetting: updateTariffSetting,
        updatePositionByIdClient: updatePositionByIdClient,
        cancelLastOrderClient: cancelLastOrderClient,
        removeFromBlackList: removeFromBlackList,
    };
};

function getQueryForGoogle(req) {
    return {
        origin: {
            lat: req.headers['origin-lat'],
            lng: req.headers['origin-lng']
        },
        destination: {
            lat: req.headers['destination-lat'],
            lng: req.headers['destination-lng']
        },
        mode: 'driving'
    };
}

function getDirections(req, res) {

    const query = getQueryForGoogle(req);
    googleMapsClient.directions(query, function (err, response) {
        if (!err) {
            if (response.json.routes.length > 0) {
                const data = response.json.routes[0].legs[0];
                userDBHelper.calculatePriceForDirection(data.distance.value, data.duration.value, 1, (error, results) => {
                    if (error === null && results !== null) {
                        console.log(results);
                        response.json.routes[0].price = results.rows;
                        sendResponse(res, response.json, null);
                    } else {
                        response.json.routes[0].price = -1;
                        sendResponse(res, response.json, error);
                    }
                });
            } else {
                sendResponse(res, response.json, 'Empty way');
            }
        } else {
            sendResponse(res, response.json, err);
        }
    });
}

function postOrder(req, res) {
    const query = getQueryForGoogle(req);
    googleMapsClient.directions(query, function (err, response) {
        if (!err) {
            const routes = response.json.routes;
            if (routes.length > 0) {
                const data = routes[0].legs[0];
                userDBHelper.calculatePriceForDirection(data.distance.value, data.duration.value, 1, (error, results) => {

                    if (error === null && results !== null) {
                        let price = -1;
                        results.rows.forEach(value => {
                            if (value.name === req.body.tariff) {
                                price = value.price;
                            }
                        });
                        if (price === -1) {
                            sendResponse(res, 'Current tariff name not found: ' + req.body.tariff, 'Bad request');
                            return;
                        }
                        routes[0].price = price;
                        const leg = routes[0].legs[0];
                        const data = [leg.start_location.lat, leg.start_location.lng, leg.end_location.lat, leg.end_location.lng, req.headers['start-city'], leg.start_address, req.headers['end-city'], leg.end_address, leg.distance.value, leg.duration.value];
                        userDBHelper.insertDirection(data, (error, results) => {
                            if (error === null && results.rows !== null) {
                                const orderData = [req.body.id_client ? req.body.id_client : req.user.user_id, req.body.contact_phone_number, results.rows[0].id, 1, req.body.comment, routes[0].price, 0, 'FINDING', null, req.body.tariff];
                                userDBHelper.insertOrder(orderData, (error, results) => {
                                    if (error === null && results.rows !== null) {
                                        routes[0].order = results.rows[0];
                                        sendResponse(res, response.json, err ? err : null);
                                    } else {
                                        sendResponse(res, null, error);
                                    }
                                });
                            } else {
                                sendResponse(res, null, error);
                            }
                        });
                    } else {
                        response.json.routes[0].price = -1;
                        sendResponse(res, response.json, error);
                    }
                });
            } else {
                sendResponse(res, response.json, 'Empty way');
            }
        } else {
            sendResponse(res, response.json, err);
        }
    });
}


function getOrder(req, res) {
    userDBHelper.getOrder([req.headers['id-order']], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getLastOrderClient(req, res) {
    userDBHelper.getLastOrderClient([req.user.user_id], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getRouteOrderById(req, res) {
    userDBHelper.getRouteOrderById([req.headers['id-order']], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function cancelLastOrderClient(req, res) {
    userDBHelper.cancelLastOrderClient([req.user.user_id], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getClient(req, res) {
    userDBHelper.getClient([req.user.user_id], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getDriver(req, res) {
    console.log(req.user);
    userDBHelper.getDriver([req.user.user_id], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getLastOrderDriver(req, res) {
    userDBHelper.getLastOrderDriver([req.user.user_id], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getOrdersByState(req, res) {
    userDBHelper.getOrdersByState([req.headers.state, '%' + req.headers.filter + '%',
        req.headers.order, req.headers.direction,
        req.headers.limit, req.headers.offset], (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getDriversTariffs(req, res) {
    userDBHelper.getDriversTariffs(req.headers, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getTransaction(req, res) {

    userDBHelper.getTransaction(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getDriverTransactions(req, res) {
    userDBHelper.getDriverTransactions([req.user.user_id], (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getDriverOrders(req, res) {
    userDBHelper.getDriverOrders([req.user.user_id], (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getClientOrders(req, res) {
    userDBHelper.getClientOrders([req.user.user_id], (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getListDrivers(req, res) {

    userDBHelper.getListDrivers(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getListClients(req, res) {
    userDBHelper.getListClients(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}


function getBlackList(req, res) {

    userDBHelper.getBlackList((sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}


function getListIssues(req, res) {
    userDBHelper.getListIssues((sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getIssue(req, res) {
    userDBHelper.getIssue((sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getMainConfigs(req, res) {

    userDBHelper.getMainConfigs((sqlError, result) => {
        sendResponse(res, result, sqlError !== null ? sqlError : null);
    });
}

function getFreeOrders(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.getFreeOrders(req.body, (sqlError, result) => {
        sendResponse(res, result, sqlError !== null ? sqlError : null);
    });
}

function getMainConfigById(req, res) {

    userDBHelper.getMainConfigById(req.headers, (sqlError, result) => {
        sendResponse(res, result, sqlError !== null ? sqlError : null);
    });
}

function insertMainConfig(req, res) {
    userDBHelper.insertMainConfig(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function insertFullRequestForDrivers(req, res) {
    console.log('req:', req.body);
    userDBHelper.insertMarkCars(req.body, (error, results) => {
        console.log('callback:', error, results);
        if (error === null) {
            req.body.id_mark = results[0].id;
            console.log('req:', req.body);
            userDBHelper.insertModelCars(req.body, (error, results) => {
                if (error === null) {
                    req.body.id_model = results[0].id;
                    console.log('req:', req.body);
                    userDBHelper.registerUserInDB(req.body.phone_number, new Date(Date.now()).toLocaleString(), (error, results) => {
                        if (error === null) {
                            req.body.id_user = results.rows[0].id;
                            userDBHelper.insertCars(req.body, (error, results) => {
                                if (error === null) {
                                    req.body.car_id = results[0].id;
                                    console.log('req:', req.body);
                                    userDBHelper.insertLicense(req.body, (error, results) => {
                                        if (error === null) {
                                            req.body.licence_id = results[0].id;
                                            console.log('req:', req.body);
                                            userDBHelper.insertPassport(req.body, (error, results) => {
                                                if (error === null) {
                                                    req.body.passport_id = results[0].id;
                                                    // req.body.id_user = req.user.user_id;
                                                    console.log('req:', req.body);
                                                    userDBHelper.insertPerson(req.body, (error, results) => {
                                                        if (error === null) {
                                                            req.body.id_person = results[0].id;
                                                            console.log('req:', req.body);
                                                            userDBHelper.insertPersonWithPhone(req.body, (error, results) => {
                                                                if (error === null) {
                                                                    req.body.id_person_with_phone = results[0].id;
                                                                    console.log('req:', req.body);
                                                                    userDBHelper.insertDriver(req.body, (error, results) => {
                                                                        if (error === null) {
                                                                            req.body.id_drivers = results[0].id;
                                                                            console.log('req:', req.body);
                                                                            userDBHelper.insertRequestForDrivers(req.body, (error, results) => {
                                                                                if (error === null) {
                                                                                    console.log('req:', req.body);
                                                                                    sendResponse(res, results[0], null);
                                                                                } else {
                                                                                    delete_drivers(req);
                                                                                    delete_passports(req);
                                                                                    delete_licenses(req);
                                                                                    delete_cars(req);
                                                                                    sendResponse(res, null, error);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            delete_passports(req);
                                                                            delete_licenses(req);
                                                                            delete_cars(req);
                                                                            sendResponse(res, 'Current driver is exists', error);
                                                                        }
                                                                    });
                                                                } else {
                                                                    delete_passports(req);
                                                                    delete_licenses(req);
                                                                    delete_cars(req);
                                                                    sendResponse(res, 'Current number is exists', error);
                                                                }
                                                            });
                                                        } else {
                                                            delete_passports(req);
                                                            delete_licenses(req);
                                                            delete_cars(req);
                                                            sendResponse(res, 'Current person is exists', error);
                                                        }
                                                    });
                                                } else {
                                                    delete_licenses(req);
                                                    delete_cars(req);
                                                    sendResponse(res, 'Current passport is exists', error);
                                                }
                                            });
                                        } else {
                                            delete_cars(req);
                                            sendResponse(res, 'Current license is exists', error);
                                        }
                                    });
                                } else {
                                    sendResponse(res, 'Current car is exists', error);
                                }
                            });
                        } else {
                            sendResponse(res, 'Current user is exists', error);
                        }
                    });
                } else {
                    sendResponse(res, 'Current model car is exists', error);
                }
            });
        } else {
            sendResponse(res, 'Current mark car is exists', error);
        }
    });
}

function updateRequestForDrivers(req, res) {
    userDBHelper.updateRequestForDrivers(req.body, (error, results) => {
        if (error === null) {
            sendResponse(res, results[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}


function delete_drivers(req) {
    userDBHelper.deleteRowById({
        field: 'drivers',
        id: req.body.id_drivers
    });
}

function delete_person_with_phone(req) {
    userDBHelper.deleteRowById({
        field: 'person_with_phone',
        id: req.body.id_person_with_phone
    });
}

function delete_person(req) {
    userDBHelper.deleteRowById({
        field: 'person',
        id: req.body.id_person
    });
}

function delete_passports(req) {
    userDBHelper.deleteRowById({
        field: 'passports',
        id: req.body.passport_id
    });
}

function delete_licenses(req) {
    userDBHelper.deleteRowById({
        field: 'licenses',
        id: req.body.licence_id
    });
}

function delete_cars(req) {
    userDBHelper.deleteRowById({
        field: 'cars',
        id: req.body.car_id
    });
}

function delete_user(req) {
    userDBHelper.deleteRowById({
        field: 'users',
        id: req.body.id_user
    });
}

function insertRequestForDrivers(req, res) {
    userDBHelper.insertRequestForDrivers(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}


function insertTransaction(req, res) {
    userDBHelper.updateBalance(req.body, (error, results) => {
        sendResponse(res, results, error);
    });
}

function insertDriverTariff(req, res) {
    userDBHelper.insertDriverTariff(req.body, (error, results) => {
        sendResponse(res, results, error);
    });
}


function insertComment(req, res) {
    userDBHelper.insertComment([req.body.order_id, req.body.comment, req.body.rating, req.body.pwp_id], (error, results) => {
        sendResponse(res, results, error !== null ? error : null);
    });
}


function insertBlackList(req, res) {
    userDBHelper.insertBlackList(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertMarkCars(req, res) {
    userDBHelper.insertMarkCars(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertCars(req, res) {
    userDBHelper.insertCars(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertModelCars(req, res) {
    userDBHelper.insertModelCars(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertPerson(req, res) {
    userDBHelper.insertPerson(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertPersonWithPhone(req, res) {
    userDBHelper.insertPersonWithPhone(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertDriver(req, res) {
    userDBHelper.insertDriver(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertPassport(req, res) {
    userDBHelper.insertPassport(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertIssueUser(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.insertIssueUser(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function insertLicense(req, res) {
    userDBHelper.insertLicense(req.body, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getRequestForDrivers(req, res) {
    userDBHelper.getRequestForDrivers(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getRequestForDriver(req, res) {
    userDBHelper.getRequestForDriver(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getPositionByIdClient(req, res) {
    userDBHelper.getPositionByIdClient(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getPositionByIdDriver(req, res) {
    userDBHelper.getPositionByIdDriver(req.headers, (sqlError, doesUserExist) => {
        sendResponse(res, doesUserExist, sqlError !== null ? sqlError : null);
    });
}

function getHistoryPositionsByIdDriver(req, res) {
    userDBHelper.getHistoryPositionsByIdDriver(req.headers, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function getPositionsAllDriver(req, res) {
    userDBHelper.getPositionsAllDriver(req.headers, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateOrder(req, res) {
    userDBHelper.updateOrder(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updatePassport(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updatePassport(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateLicense(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updateLicense(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateCar(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updateCar(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updatePersonDriver(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updatePersonDriver(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updatePhoneEmailDriver(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updatePhoneEmailDriver(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateOrderDriver(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updateOrderDriver(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updatePositionByIdClient(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updatePositionByIdClient(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateIssueAdmin(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updateIssueAdmin(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updatePositionByIdDriver(req, res) {
    req.body.user_id = req.user.user_id;
    userDBHelper.updatePositionByIdDriver(req.body, (error, results) => {
        if (error === null && results.rows !== null) {
            sendResponse(res, results.rows[0], null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateTariffSetting(req, res) {
    userDBHelper.updateTariffSetting(req.body, (error, results) => {
        if (error === null && results !== null) {
            sendResponse(res, results, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function removeFromBlackList(req, res) {
    userDBHelper.removeFromBlackList(req.headers, (error, results) => {
        if (error === null) {
            sendResponse(res, results, null);
        } else {
            sendResponse(res, null, error);
        }
    });
}

function updateMainConfig(req, res) {
    if (req.body !== null) {
        userDBHelper.updateMainConfig(req.body, (error, results) => {
            if (error === null && results !== null) {
                sendResponse(res, results, null);
            } else {
                sendResponse(res, null, error);
            }
        });
    } else {
        sendResponse(res, null, 'value not found');
    }
}

//sends a response created out of the specified parameters to the client.
//The typeOfCall is the purpose of the client's api call
function sendResponse(res, message, error) {
    res
        .status(error === null ? 200 : 400)
        .json(error !== null ? error : message);
}
