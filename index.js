//MARK: --- REQUIRE MODULES
const fs = require('fs');
const portHttps = 10443;
const portHttp = 10080;
const https = require('https');
const http = require('http');
const cors = require('cors');
const cluster = require('cluster');

const mySqlConnection = require('./databaseHelpers/mySqlWrapper');
const accessTokenDBHelper = require('./databaseHelpers/accessTokensDBHelper')(mySqlConnection);
const userDBHelper = require('./databaseHelpers/userDBHelper')(mySqlConnection);
const oAuthModel = require('./authorisation/accessTokenModel')(userDBHelper, accessTokenDBHelper);
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const oAuth2Server = require('node-oauth2-server');
const express = require('express');
const expressApp = express();

expressApp.oauth = oAuth2Server({
    model: oAuthModel,
    grants: ['password', 'refresh_token'],
    debug: true
});
const restrictedAreaRoutesMethods = require('./restrictedArea/restrictedAreaRoutesMethods.js');
const restrictedAreaRoutes = require('./restrictedArea/restrictedAreaRoutes.js')(express.Router(), expressApp, restrictedAreaRoutesMethods);
const uploadRoutesMethods = require('./upload/uploadRoutesMethods.js')(userDBHelper);
const uploadRoutes = require('./upload/uploadRoutes.js')(express.Router(), expressApp, uploadRoutesMethods);
const authRoutesMethods = require('./authorisation/authRoutesMethods')(userDBHelper, expressApp);
const authRoutes = require('./authorisation/authRoutes')(express.Router(), expressApp, authRoutesMethods);
const queryRoutesMethods = require('./dataquery/queryRoutesMethods')(userDBHelper);
const queryRoutes = require('./dataquery/queryRoutes')(express.Router(), queryRoutesMethods);
const bodyParser = require('body-parser');

const MY_SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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';
const slack = require('slack-notify')(MY_SLACK_WEBHOOK_URL);
const workers = process.env.WORKERS || require('os').cpus().length;

//MARK: --- REQUIRE MODULES

//MARK: --- INITIALISE MIDDLEWARE & ROUTES

expressApp.use(logger('dev'));

expressApp.use(cookieParser());

expressApp.use(cors());

//Этот пакет помогает защищать Express-сервера, управляя HTTP-заголовками. Он, помимо прочего, добавляет HSTS, убирает заголовок X-Powered-By и устанавливает заголовок X-Frame-Options для защиты от кликджекинга.
expressApp.use(require('helmet')());

//set the bodyParser to parse the urlencoded post data
expressApp.use(bodyParser.urlencoded({extended: true}));
expressApp.use(bodyParser.json());

//set the oAuth errorHandler
expressApp.use(expressApp.oauth.errorHandler());

//set the authRoutes for registration and & login requests
expressApp.use('/auth', authRoutes);

//set the authRoutes for registration and & login requests
expressApp.use('/query', expressApp.oauth.authorise(), queryRoutes);

//set the authRoutes for registration and & login requests
expressApp.all('/', function (req, res) {
    res.redirect('http://aziron.ru/');
});

//set the restrictedAreaRoutes used to demo the accesiblity or routes that ar OAuth2 protected
expressApp.use('/restrictedArea', restrictedAreaRoutes);

//set the restrictedAreaRoutes used to demo the accesiblity or routes that ar OAuth2 protected
expressApp.use('/upload', uploadRoutes);

expressApp.use(function (err, req, res, next) {
    console.error('ERROR: ', err);
    res.status(500);
    if (err !== null) {
        if (err.name === 'OAuth2Error') {
            if (err.code !== null) {
                res.status(err.code);
            } else {
                res.status(404);
            }
        }
    }
    res.send((err === null) ? 'Something broke!' : err);
});

//MARK: --- INITIALISE MIDDLEWARE & ROUTES

// init the server
if (cluster.isMaster) {

    //INIT DB
    console.log('Init db');
    const fs = require('fs');
    const path = require('path');
    const sqlInit = fs.readFileSync(path.join(__dirname, './databaseHelpers/dump.sql')).toString();
    mySqlConnection.query(sqlInit, [], (result) => {
    });
    ////////////


    const cron = require('node-cron');

    cron.schedule('1 * * * *', () => {
        console.log('Scheduler working');

        userDBHelper.updateStateOldOrder((error, results) => {
            if (error === null) {
                console.log('database updated success');
            } else {
                console.log('database updated failed :' + error);
            }
        });
        userDBHelper.removeOldPositionsFromPositions((error, results) => {
            if (error === null) {
                console.log('database deleted old positions success');
            } else {
                console.log('database updated failed :' + error);
            }
        });
    });

    console.log('start cluster with %s workers', workers);

    for (var i = 0; i < workers; ++i) {
        const worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    }

    Object.values(cluster.workers).forEach(worker => {
        worker.on('message', message => {
            slack.bug(message); // Posts to #bugs by default
        });
    });

    cluster.on('exit', function (worker) {
        console.log('worker %s stop.', worker.process.pid);
        cluster.fork();
    });
} else {
    http.createServer(expressApp)
        .listen(portHttp, function () {
            console.log('listening http on port ' + portHttp + '! Go to http://' + 'node' + ':' + portHttp);
        });

    https.createServer({
        key: fs.readFileSync('ssl/server.key'),
        cert: fs.readFileSync('ssl/server.cert')
    }, expressApp)
        .listen(portHttps, function () {
            console.log('listening https on port ' + portHttps + '! Go to https://' + 'node' + ':' + portHttps);
        });
}

process.on('uncaughtException', function (err) {

    console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
    console.error(err.stack);
    if (process.send) {
        process.send((new Date).toUTCString() + ' : ' + err.stack);
    }
    // process.signalCode();
    // process.errorValue = err.stack;
    process.exit(1);
});