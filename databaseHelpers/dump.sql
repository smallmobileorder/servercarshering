--
-- PostgreSQL database dump
--

create schema if not exists public;
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;
create EXTENSION IF NOT EXISTS plpgsql with SCHEMA pg_catalog;
SET default_tablespace = '';

SET default_with_oids = false;


-- drop table if exists black_list cascade;
--
-- drop table if exists cities_users cascade;
--
-- drop table if exists oauth_access_tokens cascade;
--
-- drop table if exists oauth_refresh_tokens cascade;
--
-- drop table if exists access_level cascade;
--
-- drop table if exists admins cascade;
--
-- drop table if exists transaction cascade;
--
-- drop table if exists drivers cascade;
--
-- drop table if exists cars cascade;
--
-- drop table if exists model_car cascade;
--
-- drop table if exists mark_car cascade;
--
-- drop table if exists orders cascade;
--
-- drop table if exists cities cascade;
--
-- drop table if exists clients cascade;
--
-- drop table if exists person_with_phone cascade;
--
-- drop table if exists person cascade;
--
-- drop table if exists users cascade;
--
-- drop table if exists push cascade;
--
-- drop table IF exists main_config cascade;
--
-- drop table IF exists directions cascade;
--
-- drop table IF exists phone_request_id cascade;
--
-- drop table IF exists comments cascade;
--
-- drop table IF exists licenses cascade;
--
-- drop table IF exists passports cascade;
--
-- drop table IF exists request_for_drivers cascade;
--
-- drop table if exists files cascade;
--
-- drop table if exists issues cascade;
--
-- drop type if exists ACCESS_VALUE cascade;
--
-- drop type if exists TYPE_OPERATION cascade;
--
-- drop type if exists TYPE_OBJECT cascade;
--
-- drop type if exists TYPE_BALANCE cascade;
--
-- drop type if exists TYPE_PUSH cascade;
--
-- drop type if exists STATE_ORDER cascade;
--
-- drop type if exists STATUS_ORDER cascade;
--
-- drop type if exists STATE_REQUEST cascade;
--
-- drop table if exists tariffs cascade;

DO
$$
    BEGIN
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'access_value') THEN
            create type ACCESS_VALUE as ENUM ('all', 'nothing', 'call');
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'state_request') THEN
            create type STATE_REQUEST as ENUM ('REJECTED' , 'ACCEPTED' , 'WAITING');
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'state_order') THEN
            create type STATE_ORDER as ENUM ('WAITING', 'TRIP', 'TRIP_TO_CLIENT', 'FINDING', 'FINISHED', 'PRELIMINARY');
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'status_order') THEN
            create type STATUS_ORDER as ENUM ('SUCCESS', 'DIFFERENT_REASON', 'DRIVER_CANCELED', 'CLIENT_CANCELED', 'TIME_CANCELED');
        END IF;

        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'type_operation') THEN
            create type TYPE_OPERATION as ENUM ('INTERNAL', 'COMMISSION', 'PAY_ORDER', 'ADD_BALANCE');
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'type_object') THEN
            create type TYPE_OBJECT as ENUM ('ORDER', 'WALLET');
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'type_balance') THEN
            create type TYPE_BALANCE as ENUM ('BALANCE_DRIVER', 'BALANCE_ORGANIZATION');
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'type_push') THEN
            create type TYPE_PUSH as ENUM ('ALL', 'CLIENTS', 'DRIVERS', 'PERSONAL');
        END IF;
    END
$$;

create table IF NOT exists users
(
    id            serial not null primary key,
    login         text   not null,
    user_password text   not null,
    unique (login)
);

create table IF NOT exists phone_request_id
(
    number     text not null primary key,
    request_id text not null
);


create table IF NOT exists oauth_access_tokens
(
    user_id      int                         not null primary key REFERENCES users (id),
    access_token text                        not null,
    expires      timestamp without time zone not null
);

create table IF NOT exists oauth_refresh_tokens
(
    user_id       int                         not null primary key REFERENCES users (id),
    refresh_token text                        not null,
    expires       timestamp without time zone not null default now()
);

create table IF NOT exists mark_car
(
    id        serial not null primary key,
    mark_name text   not null unique
);


create table IF NOT exists model_car
(
    id         serial not null primary key,
    model_name text   not null,
    id_mark    INT    not null REFERENCES mark_car (id),
    unique (model_name, id_mark)
);


create table IF NOT exists cars
(
    id                   serial not null primary key,
    car_number           text   not null unique,
    vin_value            text   not null unique,
    color                text,
    id_model             INT    NOT NULL REFERENCES model_car (id),
    year_made            INT,
    photo_register_back  text,
    photo_register_front text,
    count_doors          INT
);


create table IF NOT exists passports
(
    id                      serial  not null primary key,
    address_value_fact      text,
    address_value_register  text    not null,
    serial_passport         INTEGER not null,
    number_passport         INT     not null,
    date_register_passport  date    not null,
    photo_passport_main     text,
    photo_passport_register text,
    unique (serial_passport, number_passport)
);


create table IF NOT exists person
(
    id             serial not null primary key,
    first_name     text   not null,
    last_name      text   not null,
    patronymic     text,
    id_user        INT    not null REFERENCES users (id) unique,
    birthday_value date,
    gender         boolean,
    passport_id    INT unique REFERENCES passports (id)
);


create table IF NOT exists person_with_phone
(
    id           serial not null primary key,
    id_person    INT    not null REFERENCES person (id) unique,
    phone_number text   not null,
    last_lat     float,
    last_lng     float,
    email        text
);

create table IF NOT exists licenses
(
    id                    serial  not null primary key,
    license_front         text,
    license_back          text,
    serial_license        INTEGER not null,
    number_license        INT     not null,
    date_register_license date    not null,
    date_end_license      date    not null,
    unique (number_license, serial_license)
);

create table IF NOT exists drivers
(
    id                   serial not null primary key,
    id_person_with_phone INT    not null REFERENCES person_with_phone (id) unique,
    car_id               INT REFERENCES cars (id),
    licence_id           INT unique REFERENCES licenses (id),
    balance              NUMERIC
);

ALTER TABLE drivers
    DROP CONSTRAINT IF EXISTS balance_check;

create table IF NOT exists clients
(
    id                   serial not null primary key,
    id_person_with_phone INT    not null REFERENCES person_with_phone (id) unique
);


create table IF NOT exists admins
(
    id        serial not null primary key,
    id_person INT    not null REFERENCES person (id) unique
);


create table IF NOT exists access_level
(
    id       serial       not null primary key,
    id_admin INT          not null REFERENCES admins (id),
    value    ACCESS_VALUE not null
);

create table IF NOT exists transaction
(
    id             serial         not null primary key,
    timestamp      TIMESTAMP      not null,
    id_driver      INT REFERENCES drivers (id),
    balance_before NUMERIC        not null,
    amount         NUMERIC        not null,
    type_operation TYPE_OPERATION not null,
    payment_system text           not null,
    type_object    TYPE_OBJECT    not null,
    type_balance   TYPE_BALANCE   not null,
    comment        text           not null
);

create table IF NOT exists black_list
(
    id          serial    not null primary key,
    id_user     INT       not null REFERENCES person_with_phone (id) unique,
    data_lock   timestamp not null,
    data_unlock timestamp,
    check ( data_unlock IS NULL OR data_unlock > data_lock )
);


create table IF NOT exists cities
(
    id   serial not null primary key,
    name text   not null unique
);

INSERT INTO "cities" (name)
VALUES ('Saint-Petersburg')
ON CONFLICT DO NOTHING;

create table IF NOT exists directions
(
    id            serial    not null primary key,
    start_lat     FLOAT     not null,
    start_lng     FLOAT     not null,
    end_lat       FLOAT     not null,
    end_lng       FLOAT     not null,
    start_city    INT       not null REFERENCES cities (id),
    start_address text,
    end_city      INT       not null REFERENCES cities (id),
    end_address   text,
    duration      TIME,
    distance      INT,
    data_create   timestamp not null
);


create table IF NOT exists tariffs
(
    id               serial  not null primary key,
    name_tariff      text    not null,
    min_price        NUMERIC not null,
    price_for_meter  NUMERIC not null,
    price_for_minute NUMERIC not null,
    city_id          INT REFERENCES cities (id),
    unique (name_tariff, city_id)
);

insert into tariffs (name_tariff, min_price, price_for_meter, price_for_minute, city_id)
VALUES ('Demo', 100.0, 0.03, 0.2, 1),
       ('Lux', 200.0, 0.05, 0.45, 1)
ON CONFLICT DO NOTHING;


create table IF NOT exists orders
(
    id                        serial      not null primary key,
    client_id                 INT REFERENCES clients (id),
    driver_id                 INT REFERENCES drivers (id),
    contact_phone_number      text,
    direction_id              INT         not null REFERENCES directions (id),
    id_city                   INT         not null REFERENCES cities (id),
    comment                   text,
    data_start_finding        timestamp            default now(),
    data_start_trip_to_client timestamp check ( data_start_trip_to_client >= data_start_finding ),
    data_start_waiting        timestamp check ( data_start_waiting >= data_start_trip_to_client ),
    data_start_trip           timestamp check ( data_start_trip >= data_start_waiting ),
    data_finish_trip          timestamp check ( data_finish_trip >= data_start_trip ),
    price                     NUMERIC     not null,
    discount_price            NUMERIC,
    state                     STATE_ORDER not null,
    status                    STATUS_ORDER,
    count_cars                INT         not null default 1
);

alter table orders
    add COLUMN if not exists id_tariff INT not null REFERENCES tariffs (id) DEFAULT 1;


create table IF NOT exists comments
(
    id       serial not null primary key,
    order_id INT    not null REFERENCES orders (id),
    comment  text,
    rating   INT
);

alter table comments
    add column if not exists id_person_with_phone INT REFERENCES person_with_phone (id);

create table IF NOT exists cities_users
(
    id      serial not null primary key,
    id_user INT    not null REFERENCES users (id),
    id_city INT    not null REFERENCES cities (id)
);

create table IF NOT exists push
(
    id         serial    not null primary key,
    timestamp  timestamp not null,
    header     text      not null,
    message    text,
    id_creator INT       not null REFERENCES admins (id),
    type_push  TYPE_PUSH not null,
    id_city    INT       not null REFERENCES cities (id)
);


create table IF NOT exists main_config
(
    id                                    serial not null primary key,
    min_balance_driver                    NUMERIC,
    max_count_halt                        INT,
    time_free_waiting                     INT,
    time_for_cancel_order                 time,
    price_for_min_waiting                 NUMERIC,
    price_for_halt                        NUMERIC,
    price_for_halt_in_way                 NUMERIC,
    time_for_cancel_order_without_driver  time,
    time_for_finish_order                 time,
    time_for_finish_order_without_rating  time,
    life_time_rates                       time,
    min_balance_driver_for_accept_order   INT,
    time_for_check_status_call_client     time,
    interval_for_check_status_call_client time,
    count_for_check_status_call_client    INT,
    cancel_order_without_call             boolean,
    call_for_up_price                     time,
    commission_for_fine                   FLOAT check ( commission_for_fine >= 0 AND commission_for_fine <= 100 ),
    amount_fine                           NUMERIC,
    id_city                               INT    not null REFERENCES cities (id) UNIQUE
);
insert into main_config (min_balance_driver, max_count_halt, time_free_waiting,
                         time_for_cancel_order, price_for_min_waiting, price_for_halt,
                         price_for_halt_in_way, time_for_cancel_order_without_driver,
                         time_for_finish_order, time_for_finish_order_without_rating,
                         life_time_rates, min_balance_driver_for_accept_order,
                         time_for_check_status_call_client, interval_for_check_status_call_client,
                         count_for_check_status_call_client, cancel_order_without_call,
                         call_for_up_price, commission_for_fine, amount_fine, id_city)
VALUES (200, 5, 1000, '12:00', 100, 100, 100, '12:00', '12:00', '12:00', '12:00', 100, '12:00',
        '12:00', 10, false, '24:00', 10, 100, 1)
ON CONFLICT DO NOTHING;

alter table person_with_phone
    add COLUMN if not exists last_angel FLOAT;

create table IF NOT exists request_for_drivers
(
    id                 serial        not null primary key,
    id_drivers         INT REFERENCES drivers (id),
    comment            text,
    date_status_change timestamp     not null default now(),
    date_create        timestamp     not null default now(),
    state              STATE_REQUEST not null default 'WAITING'
);

create table IF NOT exists files
(
    id               serial    not null primary key,
    file_name_origin text      not null,
    file_name_saved  text      not null,
    user_id          INT       not null,
    date_upload      TIMESTAMP not null default now()
);

create table IF NOT exists positions
(
    id        serial    not null primary key,
    driver_id INT       not null references drivers (id),
    last_lat  float     not null,
    last_lng  float     not null,
    date      timestamp not null default now()
);

create table IF NOT exists issues
(
    id               serial    not null primary key,
    text_user        text      not null,
    text_answer      text,
    user_query_id    INT       not null REFERENCES person_with_phone (id),
    user_answer_id   INT REFERENCES person (id),
    timestamp_create TIMESTAMP not null DEFAULT now(),
    timestamp_answer TIMESTAMP
);

alter table drivers
    add column if not exists tariff_id INT not null default 1 REFERENCES tariffs (id);

DROP FUNCTION IF EXISTS calculateprice(integer, integer, integer);
CREATE OR REPLACE FUNCTION calculatePrice(distance INT, time_way INT, city INT)
    RETURNS TABLE
            (
                name  text,
                price NUMERIC
            )
AS
$$
DECLARE
BEGIN

    BEGIN
        CREATE TEMPORARY TABLE tariffs_with_price
        (
            name_t      text,
            price_t     NUMERIC,
            min_price_t NUMERIC
        );
    EXCEPTION
        WHEN OTHERS THEN
            TRUNCATE TABLE tariffs_with_price; -- TRUNCATE if the table already exists within the session.
    END;
    INSERT INTO tariffs_with_price
    SELECT name_tariff,
           distance * price_for_meter + time_way * price_for_minute as dpfmtwpfm,
           min_price
    FROM tariffs
    WHERE city = tariffs.city_id;
    UPDATE tariffs_with_price
    SET price_t = min_price_t
    WHERE price_t < min_price_t;
    RETURN QUERY SELECT name_t, price_t FROM tariffs_with_price;
END;
$$ LANGUAGE plpgsql;
select *
FROM calculatePrice(100, 12, 1);

CREATE OR REPLACE FUNCTION updateBalance(timestamp_ TIMESTAMP, id_driver_ INT, amount_ NUMERIC,
                                         type_operation_ TYPE_OPERATION, payment_system_ text,
                                         type_object_ TYPE_OBJECT,
                                         type_balance_ TYPE_BALANCE,
                                         comment_ text) RETURNS transaction AS
$$
DECLARE
    old_balance NUMERIC;
    result      record;
BEGIN

    UPDATE
        drivers x
    SET balance = x.balance + amount_
    FROM drivers y
    WHERE x.id = y.id
      AND x.id = id_driver_
    RETURNING y.balance INTO old_balance;

    INSERT
    INTO transaction (timestamp, id_driver, balance_before, amount,
                      type_operation, payment_system, type_object,
                      type_balance, comment)
    VALUES (timestamp_, id_driver_, old_balance, amount_, type_operation_, payment_system_,
            type_object_, type_balance_, comment_)
    RETURNING * INTO result;
    RETURN result;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_state_old_order()
    returns boolean
AS
$$
BEGIN
    UPDATE
        orders
    SET state  = 'FINISHED',
        status = 'TIME_CANCELED'
    WHERE state <> 'FINISHED'
      AND data_start_finding + (SELECT time_for_cancel_order
                                FROM main_config
                                WHERE orders.id_city = main_config.id_city) <
          now();
    return true;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_old_positions_from_positions()
    returns boolean
AS
$$
BEGIN
    DELETE
    FROM positions
    WHERE date < now() - interval '7 days';
    return true;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_date_order()
    RETURNS trigger AS
$$
BEGIN
    CASE NEW.state
        WHEN 'FINDING' THEN NEW.data_start_finding := now();
        WHEN 'TRIP_TO_CLIENT' THEN NEW.data_start_trip_to_client := now();
        WHEN 'WAITING' THEN NEW.data_start_waiting := now();
        WHEN 'TRIP' THEN NEW.data_start_trip := now();
        WHEN 'FINISHED' THEN BEGIN
            IF (OLD.data_finish_trip IS NULL) THEN
                BEGIN
                    NEW.data_finish_trip := now();
                    IF (NEW.status = 'SUCCESS') THEN
                        PERFORM updateBalance(NEW.data_finish_trip,
                                              NEW.driver_id,
                                              (- (SELECT commission_for_fine / 100 as cff
                                                  FROM main_config
                                                  WHERE main_config.id_city = NEW.id_city) *
                                               NEW.price)::NUMERIC,
                                              'COMMISSION'::type_operation,
                                              'cash'::text,
                                              'ORDER'::type_object,
                                              'BALANCE_DRIVER'::type_balance,
                                              'auto') as uB;

                    END IF;
                END;
            END IF;
        END;
        END CASE;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_date ON orders;

CREATE TRIGGER update_date
    BEFORE UPDATE
    ON orders
    FOR EACH ROW
    WHEN (OLD.state IS DISTINCT FROM NEW.state)
EXECUTE PROCEDURE public.update_date_order();


CREATE OR REPLACE FUNCTION add_position_in_history_fn()
    RETURNS trigger AS
$$
BEGIN
    BEGIN
        INSERT INTO positions (driver_id, last_lat, last_lng)
        VALUES ((SELECT id
                 FROM drivers
                 WHERE drivers.id_person_with_phone = NEW.id), NEW.last_lat, NEW.last_lng)
        ON CONFLICT DO NOTHING;
    EXCEPTION
        WHEN OTHERS THEN
    END;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS add_position_in_history_tr ON person_with_phone;

CREATE TRIGGER add_position_in_history_tr
    BEFORE UPDATE
    ON person_with_phone
    FOR EACH ROW
    WHEN (OLD.last_lat IS DISTINCT FROM NEW.last_lat OR OLD.last_lng IS DISTINCT FROM NEW.last_lng)
EXECUTE PROCEDURE public.add_position_in_history_fn();

-- INSERT INTO users(login, user_password) VALUES ('qwe', 'qwe');
-- INSERT INTO person(first_name, last_name, patronymic, id_user) VALUES ('q', 'w', 'e', 1);
-- INSERT INTO person_with_phone(id_person, phone_number, last_lat, last_lng, email) VALUES (1, '11111111', 12.2, 123.3, 'qwe');
-- INSERT INTO mark_car(mark_name) VALUES ('qwe');
-- INSERT INTO model_car(model_name, id_mark) VALUES ('qwe',1);
-- INSERT INTO cars(car_number, vin_value, color, id_model, year_made, photo_register_back, photo_register_front, count_doors) VALUES ('qwe','et','tyu',1,123,'sdf','sdf', 3);
-- INSERT INTO licenses(serial_license, number_license, date_register_license, date_end_license) VALUES (2121,32312,'10.10.2020', '12.10.2030');
-- INSERT INTO drivers(id_person_with_phone, car_id, licence_id, balance) VALUES (1, 1, 1, 123.3);
