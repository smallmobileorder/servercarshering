// eslint-disable-next-line import/no-commonjs
module.exports = {
    query: query
};

const config = {
    host: 'postgresdb',
    // host: 'localhost',
    // host: 'ec2-3-136-159-160.us-east-2.compute.amazonaws.com',
    max: 20,
    idleTimeoutMillis: 1000,
    connectionTimeoutMillis: 1000,
    user: 'postgresdocker',
    database: 'database',
    password: 'postgresdocker',
    port: 5432                  //Default port, change it if needed
};

//get the pg object
// eslint-disable-next-line import/no-commonjs
const pg = require('pg');

//object which holds the connection to the db
const pool = new pg.Pool(config);


/**
 * executes the specified sql query and provides a callback which is given
 * with the results in a DataResponseObject
 *
 * @param queryString
 * @param fields
 * @param callback - takes a DataResponseObject
 */
function query(queryString, fields, callback) {
    // console.log('start');

    pool.connect(function (error, client, done) {
        if (error) {
            console.log('error1');
            callback(error, null);
        } else {
            client.query(queryString, fields, function (error, results) {
                //call `done()` to release the client back to the pool
                done();
                // console.log('query: ', queryString);
                // console.log('fields: ', fields);
                // console.log('results: ', results);
                if (error) {
                    console.log(error);
                    callback(error, null);
                } else {
                    // console.log('norm');
                    callback(error, results);
                }
            });
        }
    });
}
