const sjcl = require('sjcl')
const validatePhoneNumber = require('validate-phone-number-node-js')

let pgConnection

module.exports = injectedPgConnection => {

  pgConnection = injectedPgConnection

  return {
    doesUserExist: doesUserExist,
    registerUserInDB: registerUserInDB,
    getUserFromCrentials: getUserFromCrentials,

    getOrder: getOrder,
    getIssue: getIssue,
    getClient: getClient,
    getDriver: getDriver,
    getBlackList: getBlackList,
    getRequestId: getRequestId,
    getTransaction: getTransaction,
    getListClients: getListClients,
    getListDrivers: getListDrivers,
    getListIssues: getListIssues,
    getMainConfigs: getMainConfigs,
    getFreeOrders: getFreeOrders,
    getDriverOrders: getDriverOrders,
    getClientOrders: getClientOrders,
    getOrdersByState: getOrdersByState,
    getDriversTariffs: getDriversTariffs,
    getMainConfigById: getMainConfigById,
    getLastOrderClient: getLastOrderClient,
    getRouteOrderById: getRouteOrderById,
    getLastOrderDriver: getLastOrderDriver,
    getRequestForDriver: getRequestForDriver,
    getRequestForDrivers: getRequestForDrivers,
    getDriverTransactions: getDriverTransactions,
    getPositionByIdClient: getPositionByIdClient,
    getPositionByIdDriver: getPositionByIdDriver,
    getPositionsAllDriver: getPositionsAllDriver,
    getHistoryPositionsByIdDriver: getHistoryPositionsByIdDriver,
    insertFile: insertFile,
    insertCars: insertCars,
    insertOrder: insertOrder,
    insertPerson: insertPerson,
    insertDriver: insertDriver,
    insertClient: insertClient,
    insertComment: insertComment,
    insertLicense: insertLicense,
    insertMarkCars: insertMarkCars,
    insertPassport: insertPassport,
    insertIssueUser: insertIssueUser,
    insertDirection: insertDirection,
    insertModelCars: insertModelCars,
    insertBlackList: insertBlackList,
    insertMainConfig: insertMainConfig,
    insertDriverTariff: insertDriverTariff,
    insertPhoneRequest: insertPhoneRequest,
    insertPersonWithPhone: insertPersonWithPhone,
    insertRequestForDrivers: insertRequestForDrivers,

    updateOrder: updateOrder,
    updateBalance: updateBalance,
    updateMainConfig: updateMainConfig,
    updateIssueAdmin: updateIssueAdmin,
    updateOrderDriver: updateOrderDriver,
    updatePassport: updatePassport,
    updateLicense: updateLicense,
    updateCar: updateCar,
    updatePersonDriver: updatePersonDriver,
    updatePhoneEmailDriver: updatePhoneEmailDriver,
    updateStateOldOrder: updateStateOldOrder,
    removeOldPositionsFromPositions: removeOldPositionsFromPositions,
    updateRequestForDrivers: updateRequestForDrivers,
    updatePositionByIdClient: updatePositionByIdClient,
    updatePositionByIdDriver: updatePositionByIdDriver,
    updateTariffSetting: updateTariffSetting,
    deleteRowById: deleteRowById,
    removeFromBlackList: removeFromBlackList,
    cancelLastOrderClient: cancelLastOrderClient,
    calculatePriceForDirection: calculatePriceForDirection,

  }
}

/**
 * attempts to register a user in the DB with the specified details.
 * it provides the results in the specified callback which takes a
 * DataResponseObject as its only parameter
 *
 * @param username
 * @param password
 * @param registrationCallback - takes a DataResponseObject
 */
function registerUserInDB (username, password, registrationCallback) {

  //create query using the data in the req.body to register the user in the db
  const shaPassword = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(password))
  console.log(shaPassword)
  const registerUserQuery = `INSERT INTO users (login, user_password)
                             VALUES ($1, $2)
                             on conflict (login) DO UPDATE SET user_password = $2
                             RETURNING *`

  //execute the query to register the user
  pgConnection.query(registerUserQuery, [username, shaPassword], registrationCallback)
}

/**
 * Gets the user with the specified username and password.
 * It provides the results in a callback which takes an:
 * an error object which will be set to null if there is no error.
 * and a user object which will be null if there is no user
 *
 * @param username
 * @param password
 * @param callback - takes an error and a user object
 */
function getUserFromCrentials (username, password, callback) {
  const shaPassword = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(password))

  //create query using the data in the req.body to register the user in the db
  const query = `SELECT *
                 FROM users
                 WHERE (login = $1 AND user_password = $2)`

  //execute the query to get the user
  pgConnection.query(query, [username, shaPassword], (error, results) => {

    //pass in the error which may be null and pass the results object which we get the user from if it is not null
    callback(false, results !== null && results.rows !== null && results.rows.length === 1 ? results.rows[0] : null)
  })
}

/**
 * Determines whether or not user with the specified userName exists.
 * It provides the results in a callback which takes 2 parameters:
 * an error object which will be set to null if there is no error, and
 * secondly a boolean value which says whether or the user exists.
 * The boolean value is set to true if the user exists else it's set
 * to false or it will be null if the results object is null.
 *
 * @param username
 * @param callback - takes an error and a boolean value indicating
 *                   whether a user exists
 */
function doesUserExist (username, callback) {

  //create query to check if the user already exists
  const doesUserExistQuery = `SELECT *
                              FROM users
                              WHERE login = $1`

  //holds the results  from the query
  const sqlCallback = (error, results) => {

    //calculate if user exists or assign null if results is null
    const doesUserExist = results !== null && results.rows !== null ? results.rows.length > 0 : null

    //check if there are any users with this username and return the appropriate value
    callback(error, doesUserExist)
  }

  //execute the query to check if the user exists
  pgConnection.query(doesUserExistQuery, [username], sqlCallback)
}

function insertDirection (data, callback) {
  const query = `INSERT INTO directions (start_lat, start_lng, end_lat, end_lng,
                                         start_city, start_address,
                                         end_city, end_address, distance, duration, data_create)
                 VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9,
                         ('00:00:00'::time + $10 * interval '1 seconds'), now())
                 RETURNING *;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function insertOrder (data, callback) {
  const query = `INSERT
                 INTO orders(client_id, contact_phone_number, direction_id, id_city,
                             comment, price, discount_price, state, status, id_tariff)
                 VALUES ((SELECT clients.id
                          from clients
                                   JOIN person_with_phone pwp
                                        on clients.id_person_with_phone = pwp.id
                                   JOIN person p on pwp.id_person = p.id
                          WHERE p.id_user = $1), $2,
                         $3, $4, $5,
                         $6, $7,
                         $8, $9,
                         (SELECT id as t_id
                          from tariffs
                          WHERE tariffs.name_tariff = $10))
                 RETURNING * `
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getBlackList (callback) {
  const query = `SELECT person.id     as ps_id,
                        black_list.id as bl_id,
                        *
                 FROM black_list
                          JOIN person_with_phone on black_list.id_user = person_with_phone.id
                          JOIN person on person_with_phone.id_person = person.id`
  pgConnection.query(query, [], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function getRequestId (data, callback) {
  const query = `SELECT *
                 FROM phone_request_id
                 WHERE (number = $1)`
  pgConnection.query(query, [data.number], (error, results) => {
    callback(error, results !== null && results.rows.length === 1 ? results.rows[0].request_id : null)
  })
}

function getTransaction (data, callback) {
  const query = `SELECT person.id        as ps_id,
                        phone_number,
                        timestamp,
                        balance_before,
                        amount,
                        balance_before + amount,
                        type_operation,
                        payment_system,
                        type_object,
                        type_balance,
                        comment,
                        count(*) OVER () AS full_count
                 FROM transaction
                          JOIN drivers ON transaction.id_driver = drivers.id
                          JOIN person_with_phone
                               on drivers.id_person_with_phone = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                 WHERE person.id || phone_number || timestamp || balance_before || amount ||
                       balance_before + amount || type_operation ||
                       payment_system || type_object || type_balance || comment ILIKE $1
                 ORDER BY (case when $3 = 'asc' AND $2 = 'person.id' then person.id end),
                          (case
                               when $3 = 'asc' AND $2 = 'phone_number' then phone_number end),
                          (case when $3 = 'asc' AND $2 = 'timestamp' then balance end),
                          (case
                               when $3 = 'asc' AND $2 = 'balance_before' then first_name end),
                          (case when $3 = 'asc' AND $2 = 'balance_after' then last_name end),
                          (case
                               when $3 = 'asc' AND $2 = 'type_operation' then patronymic end),
                          (case
                               when $3 = 'asc' AND $2 = 'payment_system' then patronymic end),
                          (case when $3 = 'asc' AND $2 = 'type_object' then patronymic end),
                          (case when $3 = 'asc' AND $2 = 'type_balance' then patronymic end),
                          (case when $3 = 'asc' AND $2 = 'comment' then patronymic end),
                          (case when $3 = 'desc' AND $2 = 'person.id' then person.id end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'phone_number'
                                   then phone_number end) DESC,
                          (case when $3 = 'desc' AND $2 = 'timestamp' then balance end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'balance_before'
                                   then first_name end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'balance_after' then last_name end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'type_operation'
                                   then patronymic end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'payment_system'
                                   then patronymic end) DESC,
                          (case when $3 = 'desc' AND $2 = 'type_object' then patronymic end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'type_balance' then patronymic end) DESC,
                          (case when $3 = 'desc' AND $2 = 'comment' then patronymic end) DESC
                 LIMIT $4 OFFSET $5;`
  pgConnection.query(query, ['%' + data.filter + '%', data.order, data.direction, data.limit, data.offset], (error, results) => {
    callback(error, results)
  })
}

function getDriverTransactions (data, callback) {
  const query = `SELECT *
                 FROM transaction
                          JOIN drivers ON transaction.id_driver = drivers.id
                          JOIN person_with_phone
                               on drivers.id_person_with_phone = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                 WHERE person.id_user = $1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getDriverOrders (data, callback) {
  const query = `SELECT *
                 FROM orders
                          JOIN drivers ON orders.driver_id = drivers.id
                          JOIN tariffs ON orders.id_tariff = tariffs.id
                          JOIN person_with_phone
                               on drivers.id_person_with_phone = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                          JOIN directions d on orders.direction_id = d.id
                 WHERE person.id_user = $1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getClientOrders (data, callback) {
  const query = `SELECT *
                 FROM orders
                          JOIN clients on orders.client_id = clients.id
                          JOIN tariffs ON orders.id_tariff = tariffs.id
                          JOIN person_with_phone
                               on clients.id_person_with_phone = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                          JOIN directions d on orders.direction_id = d.id
                 WHERE person.id_user = $1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function insertPhoneRequest (data, callback) {
  const query = `INSERT
                 INTO phone_request_id (number, request_id)
                 VALUES ($1, $2)
                 ON CONFLICT (number) DO UPDATE SET request_id = $2;`
  pgConnection.query(query, data, (error) => {
    callback(error)
  })
}

function insertComment (data, callback) {
  const query = `INSERT
                 INTO comments (order_id, comment, rating, id_person_with_phone)
                 VALUES ($1, $2, $3, $4)
                 RETURNING *;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertMarkCars (data, callback) {
  const query = `INSERT
                 INTO mark_car(mark_name)
                 VALUES ($1)
                 ON CONFLICT (mark_name) DO UPDATE SET mark_name = $1
                 RETURNING *;`
  pgConnection.query(query, [data.mark_name], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertCars (data, callback) {
  const query = `INSERT
                 INTO cars(car_number, vin_value, color, id_model, year_made, photo_register_back,
                           photo_register_front, count_doors)
                 VALUES ($1, $2, $3, $4,
                         $5, $6,
                         $7, $8)
                 RETURNING *;`
  pgConnection.query(query, [data.car_number, data.vin_value, data.color, data.id_model, data.year_made,
    data.photo_register_back, data.photo_register_front,
    data.count_doors], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertModelCars (data, callback) {
  const query = `INSERT
                 INTO model_car(model_name, id_mark)
                 VALUES ($1, $2)
                 ON CONFLICT (model_name, id_mark) DO UPDATE SET model_name = $1
                 RETURNING *;`
  pgConnection.query(query, [data.model_name, data.id_mark], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertPerson (data, callback) {
  const query = `INSERT
                 INTO person (first_name, last_name, patronymic, id_user, birthday_value, gender,
                              passport_id)
                 VALUES ($1, $2, $3, $4, $5, $6, $7)
                 ON CONFLICT (id_user) DO UPDATE
                     SET first_name     = CASE
                                              WHEN person.passport_id IS NULL THEN $1
                                              ELSE person.first_name END,
                         last_name      = CASE
                                              WHEN person.passport_id IS NULL THEN $2
                                              ELSE person.last_name END,
                         patronymic     = CASE
                                              WHEN person.passport_id IS NULL THEN $3
                                              ELSE person.patronymic END,
                         birthday_value = CASE
                                              WHEN person.passport_id IS NULL THEN $5
                                              ELSE person.birthday_value END,
                         gender         = CASE
                                              WHEN person.passport_id IS NULL THEN $6
                                              ELSE person.gender END,
                         passport_id    = CASE
                                              WHEN person.passport_id IS NULL THEN $7
                                              ELSE person.passport_id END
                 RETURNING * `
  pgConnection.query(query, [data.first_name, data.last_name, data.patronymic, data.id_user, data.birthday_value, data.gender, data.passport_id], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertPersonWithPhone (data, callback) {
  const result = validatePhoneNumber.validate(data.phone_number)
  if (result) {
    const query = `INSERT
                   INTO person_with_phone (id_person, phone_number, email)
                   VALUES ($1, $2, $3)
                   ON CONFLICT (id_person) DO UPDATE
                       SET id_person    = CASE
                                              WHEN person_with_phone.email IS NULL THEN $1
                                              ELSE person_with_phone.id_person END,
                           phone_number = CASE
                                              WHEN person_with_phone.email IS NULL THEN $2
                                              ELSE person_with_phone.phone_number END,
                           email        = CASE
                                              WHEN person_with_phone.email IS NULL THEN $3
                                              ELSE person_with_phone.email END
                   RETURNING * `
    pgConnection.query(query, [data.id_person, data.phone_number, data.email], (error, results) => {
      callback(error, results !== null && results.rows !== null ? results.rows : null)
    })
  } else {
    callback('Uncorrected phone number: ' + data.phone_number, result)
  }
}

function insertClient (data, callback) {
  const query = `INSERT
                 INTO clients (id_person_with_phone)
                 VALUES ($1)
                 ON CONFLICT (id_person_with_phone) DO NOTHING
                 RETURNING * `
  pgConnection.query(query, [data.id_person_with_phone], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertDriver (data, callback) {
  const query = `INSERT
                 INTO drivers (id_person_with_phone, car_id, licence_id, balance, tariff_id)
                 VALUES ($1, $2, $3, $4, $5)
                 RETURNING * `
  pgConnection.query(query, [data.id_person_with_phone, data.car_id, data.licence_id, data.balance, data.tariff_id], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertPassport (data, callback) {
  const query = `INSERT
                 INTO passports (address_value_fact, address_value_register, serial_passport,
                                 number_passport, date_register_passport, photo_passport_main,
                                 photo_passport_register)
                 VALUES ($1, $2, $3, $4, $5, $6, $7)
                 RETURNING * `
  pgConnection.query(query, [data.address_value_fact, data.address_value_register, data.serial_passport,
    data.number_passport, data.date_register_passport, data.photo_passport_main,
    data.photo_passport_register], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertLicense (data, callback) {
  const query = `INSERT
                 INTO licenses (license_front, license_back, serial_license, number_license,
                                date_register_license, date_end_license)
                 VALUES ($1, $2, $3, $4, $5, $6)
                 RETURNING * `
  pgConnection.query(query, [data.license_front, data.license_back, data.serial_license, data.number_license,
    data.date_register_license, data.date_end_license], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function insertBlackList (data, callback) {
  const query = `INSERT
                 INTO black_list(id_user, data_lock, data_unlock)
                 VALUES ($1, $2, $3)
                 RETURNING * `
  pgConnection.query(query, [data.person, data.data_lock, data.data_unlock], (error, results) => {//person.id
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function removeFromBlackList (data, callback) {
  const query = `DELETE
                 FROM black_list
                 WHERE id_user = $1
                 RETURNING * `
  pgConnection.query(query, [data.person], (error, results) => {//person.id
    callback(error, results)
  })
}

function deleteRowById (data) {
  const query = `DELETE FROM ${data.field} WHERE id = $1::INT 
                   RETURNING * `
  pgConnection.query(query, [data.id], () => {
  })
}

function insertRequestForDrivers (data, callback) {
  const query = `INSERT
                 INTO request_for_drivers(id_drivers, comment, date_status_change, date_create,
                                          state)
                 VALUES ($1, $2, $3, $4, $5)
                 RETURNING * `
  pgConnection.query(query, [data.id_drivers, data.comment,
    data.date_status_change, data.date_create,
    data.state], (error, results) => {
    callback(error, results !== null && results.rows !== null ? results.rows : null)
  })
}

function updateRequestForDrivers (data, callback) {
  const query = `
      UPDATE
          request_for_drivers
      SET state              = $2,
          comment            = $3,
          date_status_change = now()
      WHERE id = $1
      RETURNING *;`
  pgConnection.query(query,
    [data.id,
      data.state,
      data.comment], (error, results) => {
      callback(error, results !== null && results.rows !== null ? results.rows : null)
    })
}

function getRequestForDrivers (data, callback) {
  const query = `SELECT *, count(*) OVER () AS full_count
                 FROM request_for_drivers
                          LEFT JOIN drivers on request_for_drivers.id_drivers = drivers.id
                          LEFT JOIN person_with_phone
                                    ON drivers.id_person_with_phone = person_with_phone.id
                          LEFT JOIN person ON person_with_phone.id_person = person.id
                          LEFT JOIN cars on drivers.car_id = cars.id
                          LEFT JOIN model_car on cars.id = model_car.id
                          LEFT JOIN mark_car on model_car.id_mark = mark_car.id
                          LEFT JOIN passports on person.passport_id = passports.id
                          LEFT JOIN licenses on drivers.licence_id = licenses.id
                 WHERE person.first_name || person.last_name ||
                       COALESCE(person.patronymic, '') || person_with_phone.phone_number ||
                       request_for_drivers.date_status_change ||
                       request_for_drivers.date_create ILIKE $1
                 ORDER BY (case
                               when $3 = 'asc' AND $2 = 'request_for_drivers.date_create'
                                   then request_for_drivers.date_create end),
                          (case
                               when $3 = 'asc' AND $2 = 'request_for_drivers.date_status_change'
                                   then request_for_drivers.date_status_change end),
                          (case
                               when $3 = 'asc' AND $2 = 'state'
                                   then state end),
                          (case when $3 = 'asc' AND $2 = 'first_name' then first_name end),
                          (case when $3 = 'asc' AND $2 = 'last_name' then last_name end),
                          (case when $3 = 'asc' AND $2 = 'patronymic' then patronymic end),
                          (case
                               when $3 = 'desc' AND $2 = 'request_for_drivers.date_create'
                                   then request_for_drivers.date_create end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'request_for_drivers.date_status_change'
                                   then request_for_drivers.date_status_change end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'state'
                                   then state end) DESC,
                          (case when $3 = 'desc' AND $2 = 'first_name' then first_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'last_name' then last_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'patronymic' then patronymic end) DESC
                 LIMIT $4 OFFSET $5;`
  pgConnection.query(query, ['%' + data.filter + '%', data.order, data.direction, data.limit, data.offset], (error, results) => {
    callback(error, results)
  })
}

function getRequestForDriver (data, callback) {
  const query = `SELECT *,
                        request_for_drivers.id as rfd_id,
                        drivers.id             as d_id,
                        person_with_phone.id   as pwp_id,
                        cars.id                as c_id,
                        model_car.id           as model_c_id,
                        mark_car.id            as mark_c_id,
                        passports.id           as p_id,
                        licenses.id            as l_id
                 FROM request_for_drivers
                          LEFT JOIN drivers on request_for_drivers.id_drivers = drivers.id
                          LEFT JOIN person_with_phone
                                    ON drivers.id_person_with_phone = person_with_phone.id
                          LEFT JOIN person ON person_with_phone.id_person = person.id
                          LEFT JOIN cars on drivers.car_id = cars.id
                          LEFT JOIN model_car on cars.id = model_car.id
                          LEFT JOIN mark_car on model_car.id_mark = mark_car.id
                          LEFT JOIN passports on person.passport_id = passports.id
                          LEFT JOIN licenses on drivers.licence_id = licenses.id
                 WHERE request_for_drivers.id = $1`
  pgConnection.query(query, [data.id], (error, results) => {
    callback(error, results)
  })
}

function insertMainConfig (data, callback) {
  const query = `INSERT
                 INTO main_config(min_balance_driver, max_count_halt, time_free_waiting,
                                  time_for_cancel_order, price_for_min_waiting,
                                  price_for_halt,
                                  price_for_halt_in_way, time_for_cancel_order_without_driver,
                                  time_for_finish_order, time_for_finish_order_without_rating,
                                  life_time_rates, min_balance_driver_for_accept_order,
                                  time_for_check_status_call_client,
                                  interval_for_check_status_call_client,
                                  count_for_check_status_call_client,
                                  cancel_order_without_call,
                                  call_for_up_price, commission_for_fine, amount_fine,
                                  id_city)
                 VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15,
                         $16,
                         $17, $18, $19, $20)
                 RETURNING * `
  pgConnection.query(query, [data.min_balance_driver, data.max_count_halt, data.time_free_waiting,
    data.time_for_cancel_order, data.price_for_min_waiting, data.price_for_halt, data.price_for_halt_in_way,
    data.time_for_cancel_order_without_driver, data.time_for_finish_order, data.time_for_finish_order_without_rating,
    data.life_time_rates, data.min_balance_driver_for_accept_order, data.time_for_check_status_call_client,
    data.interval_for_check_status_call_client, data.count_for_check_status_call_client, data.cancel_order_without_call,
    data.call_for_up_price, data.commission_for_fine, data.amount_fine, data.id_city], (error, results) => {
    callback(error, results)
  })
}

function getListClients (data, callback) {
  const query = `SELECT *,
                        count(*) OVER () AS full_count,
                        black_list.id    as bl_id,
                        person.id        as ps_id,
                        clients.id       as c_id
                 FROM clients
                          JOIN person_with_phone
                               ON clients.id_person_with_phone = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                          LEFT JOIN black_list on person_with_phone.id = black_list.id_user
                 WHERE person.first_name || person.last_name ||
                       COALESCE(person.patronymic, '') || person_with_phone.phone_number ILIKE $1
                 ORDER BY (case when $3 = 'asc' AND $2 = 'person.id' then person.id end),
                          (case
                               when $3 = 'asc' AND $2 = 'phone_number' then phone_number end),
                          (case
                               when $3 = 'asc' AND $2 = 'black_list.id'
                                   then black_list.id end),
                          (case when $3 = 'asc' AND $2 = 'first_name' then first_name end),
                          (case when $3 = 'asc' AND $2 = 'last_name' then last_name end),
                          (case when $3 = 'asc' AND $2 = 'patronymic' then patronymic end),
                          (case when $3 = 'desc' AND $2 = 'person.id' then person.id end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'phone_number'
                                   then phone_number end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'black_list.id'
                                   then black_list.id end) DESC,
                          (case when $3 = 'desc' AND $2 = 'first_name' then first_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'last_name' then last_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'patronymic' then patronymic end) DESC
                 LIMIT $4 OFFSET $5;`
  pgConnection.query(query, ['%' + data.filter + '%', data.order, data.direction, data.limit, data.offset], (error, results) => {
    callback(error, results)
  })
}

function getOrder (data, callback) {
  const query = `SELECT orders.id         as orders_id,
                        pd.first_name     as pd_first_name,
                        pd.last_name      as pd_last_name,
                        pd.patronymic     as pd_patronymic,
                        pc.first_name     as pc_first_name,
                        pc.last_name      as pc_last_name,
                        pc.patronymic     as pc_patronymic,
                        pwpc.phone_number as pc_phone_number,
                        pwpd.phone_number as pd_phone_number,
                        *
                 FROM orders
                          JOIN directions ON orders.direction_id = directions.id
                          LEFT JOIN drivers on orders.driver_id = drivers.id
                          LEFT JOIN tariffs ON drivers.tariff_id = tariffs.id
                          LEFT JOIN cars on drivers.car_id = cars.id
                          LEFT JOIN person_with_phone pwpd
                                    on drivers.id_person_with_phone = pwpd.id
                          LEFT JOIN person pd on pwpd.id_person = pd.id
                          LEFT JOIN clients on orders.client_id = clients.id
                          LEFT JOIN person_with_phone pwpc
                                    on clients.id_person_with_phone = pwpc.id
                          LEFT JOIN person pc on pwpc.id_person = pc.id
                 WHERE orders.id = $1`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getLastOrderDriver (data, callback) {
  const query = `SELECT orders.id as orders_id, *
                 FROM orders
                          JOIN drivers d on orders.driver_id = d.id
                          JOIN tariffs ON d.tariff_id = tariffs.id
                          JOIN person_with_phone pwp
                               on d.id_person_with_phone = pwp.id
                          JOIN person p on pwp.id_person = p.id
                 WHERE p.id_user = $1
                 order by orders.data_start_finding DESC
                 LIMIT 1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function insertFile (data, callback) {
  const query = `INSERT
                 INTO files(user_id, file_name_origin, file_name_saved)
                 VALUES ($1, $2, $3)
                 RETURNING *;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getLastOrderClient (data, callback) {
  const query = `SELECT orders.id         as orders_id,
                        *,
                        pd.first_name     as pd_first_name,
                        pd.last_name      as pd_last_name,
                        pc.first_name     as pc_first_name,
                        pc.last_name      as pc_last_name,
                        pwpd.phone_number as pd_phone_number,
                        pwpc.phone_number as pc_phone_number,
                        pwpd.last_lat     as pwpd_last_lat,
                        pwpd.last_lng     as pwpd_last_lng,
                        pwpc.last_lat     as pwpc_last_lat,
                        pwpc.last_lng     as pwpc_last_lng,
                        pwpd.last_angel   as pwpd_last_angle
                 FROM orders
                          JOIN clients c on orders.client_id = c.id
                          JOIN person_with_phone pwpc
                               on c.id_person_with_phone = pwpc.id
                          JOIN person pc on pwpc.id_person = pc.id
                          JOIN directions on orders.direction_id = directions.id
                          LEFT JOIN drivers d on orders.driver_id = d.id
                          LEFT JOIN tariffs ON d.tariff_id = tariffs.id
                          LEFT JOIN cars car on d.car_id = car.id
                          LEFT JOIN model_car mc ON car.id_model = mc.id
                          LEFT JOIN mark_car ON mc.id_mark = mark_car.id
                          LEFT JOIN person_with_phone pwpd
                                    on d.id_person_with_phone = pwpd.id
                          LEFT JOIN person pd on pwpd.id_person = pd.id
                 WHERE pc.id_user = $1
                 order by orders.data_start_finding DESC
                 LIMIT 1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getRouteOrderById (data, callback) {
  const query = `SELECT *
                 FROM orders
                          JOIN drivers d on orders.driver_id = d.id
                          JOIN positions p on d.id = p.driver_id
                 WHERE orders.id = $1
                   AND p.date >= orders.data_start_finding
                   AND p.date <=
                       (SELECT CASE
                                   WHEN orders.data_finish_trip IS NULL
                                       THEN now()
                                   ELSE orders.data_finish_trip end);`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function cancelLastOrderClient (data, callback) {
  const query = `UPDATE
                     orders
                 SET state  = 'FINISHED',
                     status = 'CLIENT_CANCELED'
                 WHERE id = (SELECT orders.id
                             FROM orders
                                      JOIN clients c on orders.client_id = c.id
                                      JOIN person_with_phone pwpc
                                           on c.id_person_with_phone = pwpc.id
                                      JOIN person pc on pwpc.id_person = pc.id
                             WHERE pc.id_user = $1
                             order by orders.data_start_finding DESC
                             LIMIT 1)
                   AND (state = 'FINDING' OR state = 'TRIP_TO_CLIENT' OR state = 'WAITING')
                 RETURNING *;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

// [req.headers.state, '%' + req.headers.filter + '%',
//     req.headers.order, req.headers.direction,
//     req.headers.limit, req.headers.offset],
function getOrdersByState (data, callback) {
  const query = `SELECT orders.id         as orders_id,
                        *,
                        pd.first_name     as pd_first_name,
                        pd.last_name      as pd_last_name,
                        pd.patronymic     as pd_patronymic,
                        pc.first_name     as pc_first_name,
                        pc.last_name      as pc_last_name,
                        pc.patronymic     as pc_patronymic,
                        pwpd.phone_number as pd_phone_number,
                        pwpc.phone_number as pc_phone_number,
                        count(*) OVER ()  AS full_count
                 FROM orders
                          JOIN directions ON orders.direction_id = directions.id
                          LEFT JOIN drivers on orders.driver_id = drivers.id
                          LEFT JOIN person_with_phone pwpd
                                    on drivers.id_person_with_phone = pwpd.id
                          LEFT JOIN person pd on pwpd.id_person = pd.id
                          LEFT JOIN clients on orders.client_id = clients.id
                          LEFT JOIN person_with_phone pwpc
                                    on clients.id_person_with_phone = pwpc.id
                          LEFT JOIN person pc on pwpc.id_person = pc.id
                 WHERE orders.state = $1
                   AND (COALESCE(pd.first_name, '') || COALESCE(pd.last_name, '') ||
                        COALESCE(pc.first_name, '') || COALESCE(pc.last_name, '') ||
                        orders.id || orders.data_start_finding ILIKE $2)
                 ORDER BY (case when $4 = 'asc' AND $3 = 'pd.first_name' then pd.first_name end),
                          (case
                               when $4 = 'asc' AND $3 = 'pd.last_name' then pd.last_name end),
                          (case
                               when $4 = 'asc' AND $3 = 'pc.first_name' then pc.first_name end),
                          (case when $4 = 'asc' AND $3 = 'pc.last_name' then pc.last_name end),
                          (case when $4 = 'asc' AND $3 = 'orders.id' then orders.id end),
                          (case
                               when $4 = 'asc' AND $3 = 'orders.data_start_finding'
                                   then orders.data_start_finding end),
                          (case
                               when $4 = 'desc' AND $3 = 'pd.first_name'
                                   then pd.first_name end) DESC,
                          (case
                               when $4 = 'desc' AND $3 = 'pd.last_name'
                                   then pd.last_name end) DESC,
                          (case
                               when $4 = 'desc' AND $3 = 'pc.first_name'
                                   then pc.first_name end) DESC,
                          (case
                               when $4 = 'desc' AND $3 = 'pc.last_name'
                                   then pc.last_name end) DESC,
                          (case
                               when $4 = 'desc' AND $3 = 'orders.data_start_finding'
                                   then orders.data_start_finding end) DESC,
                          (case
                               when $4 = 'desc' AND $3 = 'orders.id'
                                   then orders.data_start_finding end) DESC
                 LIMIT $5 OFFSET $6;
  ;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getFreeOrders (data, callback) {
  const query = `SELECT orders.id         as orders_id,
                        *,
                        pc.first_name     as pc_first_name,
                        pc.last_name      as pc_last_name,
                        pc.patronymic     as pc_patronymic,
                        pwpc.phone_number as pc_phone_number
                 FROM orders
                          JOIN directions ON orders.direction_id = directions.id
                          JOIN clients on orders.client_id = clients.id
                          JOIN person_with_phone pwpc
                               on clients.id_person_with_phone = pwpc.id
                          JOIN person pc on pwpc.id_person = pc.id
                          JOIN person p on $1 = p.id_user
                          JOIN person_with_phone pwpd
                               on p.id = pwpd.id_person
                          JOIN drivers d on pwpd.id = d.id_person_with_phone
                 WHERE orders.state = 'FINDING'
                   AND d.tariff_id = orders.id_tariff
                   AND orders.data_start_finding + (SELECT time_for_cancel_order
                                                    FROM main_config
                                                    WHERE orders.id_city = main_config.id_city) >
                       now()
                   AND d.balance > (SELECT min_balance_driver_for_accept_order
                                    FROM main_config
                                    WHERE orders.id_city = main_config.id_city);`
  pgConnection.query(query, [data.user_id], (error, results) => {
    callback(error, results)
  })
}

function getMainConfigs (callback) {
  const query = `SELECT *
                 FROM main_config
                          JOIN cities ON main_config.id_city = cities.id`
  pgConnection.query(query, [], (error, results) => {
    callback(error, results)
  })
}

function getMainConfigById (data, callback) {
  const query = `SELECT *
                 FROM main_config
                 WHERE main_config.id_city = $1`
  pgConnection.query(query, [data['city-id']], (error, results) => {
    callback(error, results)
  })
}

function calculatePriceForDirection (distance, time_way, city, callback) {
  const query = 'SELECT * FROM calculateprice($1, $2, $3);'
  pgConnection.query(query, [distance, time_way, city], (error, results) => {
    callback(error, results)
  })
}

function updateBalance (data, callback) {
  const query = 'SELECT updateBalance($1, $2, $3, $4, $5, $6, $7, $8);'
  pgConnection.query(query, [data.timestamp, data.id_driver, data.amount,
    data.type_operation, data.payment_system, data.type_object,
    data.type_balance, data.comment], (error, results) => {
    callback(error, results)
  })
}

function updateStateOldOrder (callback) {
  const query = 'SELECT * FROM update_state_old_order();'
  pgConnection.query(query, [], (error, results) => {
    callback(error, results)
  })
}

function removeOldPositionsFromPositions (callback) {
  const query = 'SELECT * FROM remove_old_positions_from_positions();'
  pgConnection.query(query, [], (error, results) => {
    callback(error, results)
  })
}

function getListDrivers (data, callback) {
  const query = `SELECT person.id        as ps_id,
                        phone_number,
                        car_number,
                        black_list.id    as bl_id,
                        drivers.id       as d_id,
                        balance,
                        first_name,
                        last_name,
                        patronymic,
                        count(*) OVER () AS full_count
                 FROM drivers
                          JOIN person_with_phone
                               ON drivers.id_person_with_phone = person_with_phone.id
                          JOIN request_for_drivers
                               ON drivers.id = request_for_drivers.id_drivers
                          JOIN person ON person_with_phone.id_person = person.id
                          JOIN cars ON drivers.car_id = cars.id
                          LEFT JOIN black_list on person_with_phone.id = black_list.id_user
                 WHERE request_for_drivers.state = 'ACCEPTED'
                   AND cars.car_number || person.first_name || person.last_name ||
                       COALESCE(person.patronymic, '') || drivers.balance ||
                       person_with_phone.phone_number ILIKE $1
                 ORDER BY (case when $3 = 'asc' AND $2 = 'person.id' then person.id end),
                          (case
                               when $3 = 'asc' AND $2 = 'phone_number' then phone_number end),
                          (case when $3 = 'asc' AND $2 = 'car_number' then car_number end),
                          (case
                               when $3 = 'asc' AND $2 = 'black_list.id'
                                   then black_list.id end),
                          (case when $3 = 'asc' AND $2 = 'balance' then balance end),
                          (case when $3 = 'asc' AND $2 = 'first_name' then first_name end),
                          (case when $3 = 'asc' AND $2 = 'last_name' then last_name end),
                          (case when $3 = 'asc' AND $2 = 'patronymic' then patronymic end),
                          (case when $3 = 'desc' AND $2 = 'person.id' then person.id end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'phone_number'
                                   then phone_number end) DESC,
                          (case when $3 = 'desc' AND $2 = 'car_number' then car_number end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'black_list.id'
                                   then black_list.id end) DESC,
                          (case when $3 = 'desc' AND $2 = 'balance' then balance end) DESC,
                          (case when $3 = 'desc' AND $2 = 'first_name' then first_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'last_name' then last_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'patronymic' then patronymic end) DESC
                 LIMIT $4 OFFSET $5;`
  pgConnection.query(query, ['%' + data.filter + '%', data.order, data.direction, data.limit, data.offset], (error, results) => {
    callback(error, results)
  })
}

function getListIssues (data, callback) {
  const query = `SELECT person.id        as ps_id,
                        issues.id        as i_id,
                        black_list.id    as bl_id,
                        count(*) OVER () AS full_count
                 FROM issues
                          JOIN person_with_phone
                               ON issues.user_query_id = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                          LEFT JOIN black_list on person_with_phone.id = black_list.id_user
                 WHERE person.first_name || person.last_name ||
                       COALESCE(person.patronymic, '') ||
                       person_with_phone.phone_number ||
                       COALESCE(person_with_phone.email, '') ILIKE $1
                 ORDER BY (case when $3 = 'asc' AND $2 = 'person.id' then person.id end),
                          (case
                               when $3 = 'asc' AND $2 = 'phone_number' then phone_number end),
                          (case
                               when $3 = 'asc' AND $2 = 'black_list.id'
                                   then black_list.id end),
                          (case when $3 = 'asc' AND $2 = 'first_name' then first_name end),
                          (case when $3 = 'asc' AND $2 = 'last_name' then last_name end),
                          (case when $3 = 'asc' AND $2 = 'email' then email end),
                          (case when $3 = 'asc' AND $2 = 'patronymic' then patronymic end),
                          (case when $3 = 'desc' AND $2 = 'person.id' then person.id end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'phone_number'
                                   then phone_number end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'black_list.id'
                                   then black_list.id end) DESC,
                          (case when $3 = 'desc' AND $2 = 'email' then email end) DESC,
                          (case when $3 = 'desc' AND $2 = 'first_name' then first_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'last_name' then last_name end) DESC,
                          (case when $3 = 'desc' AND $2 = 'patronymic' then patronymic end) DESC
                 LIMIT $4 OFFSET $5;`
  pgConnection.query(query, ['%' + data.filter + '%', data.order, data.direction, data.limit, data.offset], (error, results) => {
    callback(error, results)
  })
}

function getIssue (data, callback) {
  const query = `SELECT pu.id         as pu_id,
                        pa.id         as pa_id,
                        issues.id     as i_id,
                        black_list.id as bl_id,
                        pu.first_name as pd_first_name,
                        pu.last_name  as pd_last_name,
                        pu.patronymic as pd_patronymic,
                        pa.first_name as pc_first_name,
                        pa.last_name  as pc_last_name,
                        pa.patronymic as pc_patronymic
                 FROM issues
                          JOIN person_with_phone pwpu
                               ON issues.user_query_id = pwpu.id
                          JOIN person pu ON pwpu.id_person = pu.id
                          LEFT JOIN person pa ON issues.user_answer_id = pa.id
                          LEFT JOIN black_list on pwpu.id = black_list.id_user
                 WHERE issues.id = $1;`
  pgConnection.query(query, [data.id], (error, results) => {
    callback(error, results)
  })
}

function getDriver (data, callback) {
  const query = `SELECT *
                 FROM drivers
                          JOIN person_with_phone
                               ON drivers.id_person_with_phone = person_with_phone.id
                          JOIN request_for_drivers
                               ON drivers.id = request_for_drivers.id_drivers
                          JOIN person ON person_with_phone.id_person = person.id
                          JOIN tariffs t ON drivers.tariff_id = t.id
                          LEFT JOIN cars ON drivers.car_id = cars.id
                          LEFT JOIN model_car ON cars.id_model = model_car.id
                          LEFT JOIN mark_car mc on model_car.id_mark = mc.id
                          LEFT JOIN licenses l ON drivers.licence_id = l.id
                          LEFT JOIN passports p ON person.passport_id = p.id
                          LEFT JOIN black_list on person_with_phone.id = black_list.id_user,
                      main_config
                 WHERE person.id_user = $1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function getClient (data, callback) {
  const query = `SELECT *
                 FROM clients
                          JOIN person_with_phone
                               ON clients.id_person_with_phone = person_with_phone.id
                          JOIN person ON person_with_phone.id_person = person.id
                          LEFT JOIN black_list on person_with_phone.id = black_list.id_user
                 WHERE person.id_user = $1;`
  pgConnection.query(query, data, (error, results) => {
    callback(error, results)
  })
}

function updateOrder (data, callback) {
  const query = `UPDATE
                     orders
                 SET state          = $2,
                     comment        = $3,
                     price          = $4,
                     discount_price = $5,
                     status         = $6
                 WHERE id = $1
                 RETURNING *;`
  pgConnection.query(query, [data.id, data.state, data.comment, data.price, data.discount_price, data.status], (error, results) => {
    callback(error, results)
  })
}

function updateOrderDriver (data, callback) {
  const query = `UPDATE
                     orders
                 SET driver_id = (SELECT drivers.id
                                  from drivers
                                           JOIN person_with_phone pwp
                                                on drivers.id_person_with_phone = pwp.id
                                           JOIN person p on pwp.id_person = p.id
                                  WHERE p.id_user = $2),
                     state     = $3,
                     status    = $4
                 WHERE id = $1
                   AND ((state = 'FINDING') OR (driver_id = (SELECT drivers.id
                                                             from drivers
                                                                      JOIN person_with_phone pwp
                                                                           on drivers.id_person_with_phone = pwp.id
                                                                      JOIN person p on pwp.id_person = p.id
                                                             WHERE p.id_user = $2)))
                 RETURNING *;`
  pgConnection.query(query, [data.id, data.user_id, data.state, data.status], (error, results) => {
    callback(error, results)
  })
}

function updatePassport (data, callback) {
  const query = `UPDATE
                     passports
                 SET address_value_fact      = $2,
                     address_value_register  = $3,
                     serial_passport         = $4,
                     number_passport         = $5,
                     date_register_passport  = $6,
                     photo_passport_main     = $7,
                     photo_passport_register = $8
                 WHERE id = (SELECT p.passport_id
                             from drivers
                                      JOIN person_with_phone pwp
                                           on drivers.id_person_with_phone = pwp.id
                                      JOIN person p on pwp.id_person = p.id
                             WHERE drivers.id = $1)
                 RETURNING *;`
  pgConnection.query(query, [
    data.driver_id,
    data.address_value_fact,
    data.address_value_register,
    data.serial_passport,
    data.number_passport,
    data.date_register_passport,
    data.photo_passport_main,
    data.photo_passport_register], (error, results) => {
    callback(error, results)
  })
}

function updateLicense (data, callback) {
  const query = `UPDATE
                     licenses
                 SET license_front         = $2,
                     license_back          = $3,
                     serial_license        = $4,
                     number_license        = $5,
                     date_register_license = $6,
                     date_end_license      = $7
                 WHERE id = (SELECT drivers.licence_id
                             from drivers
                             WHERE drivers.id = $1)
                 RETURNING *;`
  pgConnection.query(query, [
    data.driver_id,
    data.license_front,
    data.license_back,
    data.serial_license,
    data.number_license,
    data.date_register_license,
    data.date_end_license], (error, results) => {
    callback(error, results)
  })
}

function updateCar (data, callback) {
  const query = `UPDATE
                     cars
                 SET car_number           = $2,
                     vin_value            = $3,
                     color                = $4,
                     id_model             = $5,
                     year_made            = $6,
                     photo_register_back  = $7,
                     photo_register_front = $8,
                     count_doors          = $9
                 WHERE id = (SELECT drivers.car_id
                             from drivers
                             WHERE drivers.id = $1)
                 RETURNING *;`
  pgConnection.query(query, [
    data.driver_id,
    data.car_number,
    data.vin_value,
    data.color,
    data.id_model,
    data.year_made,
    data.photo_register_back,
    data.photo_register_front,
    data.count_doors], (error, results) => {
    callback(error, results)
  })
}

function updatePersonDriver (data, callback) {
  const query = `UPDATE
                     person
                 SET first_name     = $2,
                     last_name      = $3,
                     patronymic     = $4,
                     birthday_value = $5,
                     gender         = $6
                 WHERE id = (SELECT pwp.id_person
                             from drivers
                                      JOIN person_with_phone pwp
                                           on drivers.id_person_with_phone = pwp.id
                             WHERE drivers.id = $1)
                 RETURNING *;`
  pgConnection.query(query, [
    data.driver_id,
    data.first_name,
    data.last_name,
    data.patronymic,
    data.birthday_value,
    data.gender], (error, results) => {
    callback(error, results)
  })
}

function updatePhoneEmailDriver (data, callback) {
  const query = `UPDATE
                     person_with_phone
                 SET phone_number = $2,
                     email        = $3
                 WHERE id = (SELECT drivers.id_person_with_phone
                             from drivers
                             WHERE drivers.id = $1)
                 RETURNING *;`
  pgConnection.query(query, [
    data.driver_id,
    data.phone_number,
    data.email], (error, results) => {
    callback(error, results)
  })
}

function updateMainConfig (data, callback) {
  const query = `UPDATE main_config
                 SET min_balance_driver                    = $1,
                     max_count_halt                        = $2,
                     time_free_waiting                     = $3,
                     time_for_cancel_order                 = $4,
                     price_for_min_waiting                 = $5,
                     price_for_halt                        = $6,
                     price_for_halt_in_way                 = $7,
                     time_for_cancel_order_without_driver  = $8,
                     time_for_finish_order                 = $9,
                     time_for_finish_order_without_rating  = $10,
                     life_time_rates                       = $11,
                     min_balance_driver_for_accept_order   = $12,
                     time_for_check_status_call_client     = $13,
                     interval_for_check_status_call_client = $14,
                     count_for_check_status_call_client    = $15,
                     cancel_order_without_call             = $16,
                     call_for_up_price                     = $17,
                     commission_for_fine                   = $18,
                     amount_fine                           = $19,
                     id_city                               = $20
                 WHERE (id = $21)
                 RETURNING * `
  pgConnection.query(query, [data.min_balance_driver, data.max_count_halt, data.time_free_waiting, data.time_for_cancel_order,
    data.price_for_min_waiting, data.price_for_halt, data.price_for_halt_in_way, data.time_for_cancel_order_without_driver,
    data.time_for_finish_order, data.time_for_finish_order_without_rating, data.life_time_rates, data.min_balance_driver_for_accept_order,
    data.time_for_check_status_call_client, data.interval_for_check_status_call_client, data.count_for_check_status_call_client,
    data.cancel_order_without_call, data.call_for_up_price, data.commission_for_fine, data.amount_fine, data.id_city, data.id], (error, results) => {
    callback(error, results)
  })
}

function getPositionByIdClient (data, callback) {
  const query = `SELECT last_lat, last_lng
                 FROM clients
                          JOIN person_with_phone pwp on clients.id_person_with_phone = pwp.id
                 WHERE clients.id = $1`
  pgConnection.query(query, [data.id], (error, results) => {
    callback(error, results)
  })
}

function getDriversTariffs (data, callback) {
  const query = `SELECT *
                 FROM tariffs
                 WHERE name_tariff ILIKE $1
                 ORDER BY (case when $3 = 'asc' AND $2 = 'tariffs.id' then tariffs.id end),
                          (case
                               when $3 = 'asc' AND $2 = 'tariffs.name_tariff'
                                   then tariffs.name_tariff end),
                          (case
                               when $3 = 'asc' AND $2 = 'tariffs.min_price'
                                   then tariffs.min_price end),
                          (case
                               when $3 = 'asc' AND $2 = 'tariffs.price_for_minute'
                                   then tariffs.price_for_minute end),
                          (case
                               when $3 = 'asc' AND $2 = 'tariffs.price_for_meter'
                                   then tariffs.price_for_meter end),
                          (case
                               when $3 = 'desc' AND $2 = 'tariffs.id'
                                   then tariffs.id end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'tariffs.name_tariff'
                                   then tariffs.name_tariff end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'tariffs.min_price'
                                   then tariffs.min_price end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'tariffs.price_for_minute'
                                   then tariffs.price_for_minute end) DESC,
                          (case
                               when $3 = 'desc' AND $2 = 'tariffs.price_for_meter'
                                   then tariffs.price_for_meter end) DESC
                 LIMIT $4 OFFSET $5;`
  pgConnection.query(query, ['%' + data.filter + '%', data.order, data.direction, data.limit, data.offset], (error, results) => {
    callback(error, results)
  })
}

function insertDriverTariff (data, callback) {
  const query = `INSERT
                 INTO tariffs(name_tariff, min_price, price_for_meter, price_for_minute, city_id)
                 VALUES ($1, $2, $3, $4, 1)
                 RETURNING *;`
  pgConnection.query(query, [data.name_tariff, data.min_price, data.price_for_meter, data.price_for_minute], (error, results) => {
    callback(error, results)
  })
}

function getPositionsAllDriver (data, callback) {
  const query = `SELECT DISTINCT ON (drivers.id) *,
                                                 p.id       as p_id,
                                                 pwp.id     as pwp_id,
                                                 drivers.id as d_id
                 FROM drivers
                          JOIN person_with_phone pwp
                               on drivers.id_person_with_phone = pwp.id
                          JOIN person p on pwp.id_person = p.id
                          JOIN positions position on drivers.id = position.driver_id
                 WHERE position.date > now() - interval '1 hour'
                 ORDER BY drivers.id, position.date DESC;`
  pgConnection.query(query, [], (error, results) => {
    callback(error, results)
  })
}

function getPositionByIdDriver (data, callback) {
  const query = `SELECT last_lat, last_lng
                 FROM drivers
                          JOIN person_with_phone pwp on drivers.id_person_with_phone = pwp.id
                 WHERE drivers.id = $1`
  pgConnection.query(query, [data.id], (error, results) => {
    callback(error, results)
  })
}

function getHistoryPositionsByIdDriver (data, callback) {
  const query = `SELECT *
                 FROM positions
                 WHERE positions.driver_id = $1
                 ORDER BY positions.date`
  pgConnection.query(query, [data.id], (error, results) => {
    callback(error, results)
  })
}

function updatePositionByIdClient (data, callback) {
  const query = `UPDATE person_with_phone
                 SET last_lat   = $2,
                     last_lng   = $3,
                     last_angel = $4
                 WHERE id = (SELECT pwp.id
                             FROM person p
                                      JOIN person_with_phone pwp on p.id = pwp.id_person
                                      JOIN clients c on pwp.id = c.id_person_with_phone
                             WHERE p.id_user = $1)`
  pgConnection.query(query, [data.user_id, data.last_lat, data.last_lng, data.last_angel], (error, results) => {
    callback(error, results)
  })
}

function updateIssueAdmin (data, callback) {
  const query = `UPDATE issues
                 SET text_answer      = $2,
                     user_answer_id   = (SELECT pwp.id
                                         FROM person p
                                                  JOIN person_with_phone pwp on p.id = pwp.id_person
                                         WHERE p.id_user = $1),
                     timestamp_answer = now()
                 WHERE id = $3`
  pgConnection.query(query, [data.user_id, data.text_answer, data.id], (error, results) => {
    callback(error, results)
  })
}

function insertIssueUser (data, callback) {
  const query = `INSERT INTO issues (text_user, user_query_id)
                 VALUES (text_user = $2,
                         user_query_id = (SELECT pwp.id
                                          FROM person p
                                                   JOIN person_with_phone pwp on p.id = pwp.id_person
                                          WHERE p.id_user = $1));`
  pgConnection.query(query, [data.user_id, data.text_user], (error, results) => {
    callback(error, results)
  })
}

function updatePositionByIdDriver (data, callback) {
  const query = `UPDATE person_with_phone
                 SET last_lat   = $2,
                     last_lng   = $3,
                     last_angel = $4
                 WHERE id = (SELECT pwp.id
                             FROM person p
                                      JOIN person_with_phone pwp on p.id = pwp.id_person
                                      JOIN drivers d on pwp.id = d.id_person_with_phone
                             WHERE p.id_user = $1)`
  pgConnection.query(query, [data.user_id, data.last_lat, data.last_lng, data.last_angel], (error, results) => {
    callback(error, results)
  })
}

function updateTariffSetting (data, callback) {
  const query = `UPDATE tariffs
                 SET name_tariff      = $2,
                     min_price        = $3,
                     price_for_minute = $4,
                     price_for_meter  = $5
                 WHERE id = $1`
  pgConnection.query(query, [data.id, data.name_tariff, data.min_price, data.price_for_minute, data.price_for_meter], (error, results) => {
    callback(error, results)
  })
}






