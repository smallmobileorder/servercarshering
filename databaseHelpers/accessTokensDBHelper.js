let mySqlConnection;
module.exports = injectedMySqlConnection => {

    mySqlConnection = injectedMySqlConnection;

    return {
        saveAccessToken: saveAccessToken,
        saveRefreshToken: saveRefreshToken,
        getUserIDFromBearerToken: getUserIDFromBearerToken,
        getUserIDFromRefreshToken: getUserIDFromRefreshToken
    };
};

/**
 * Saves the accessToken against the user with the specified userID
 * It provides the results in a callback which takes 2 parameters:
 *
 * @param accessToken
 * @param userID
 * @param callback - takes either an error or null if we successfully saved the accessToken
 */
function saveAccessToken(accessToken, userID, expires, callback) {
    const getUserQuery = `INSERT INTO oauth_access_tokens (user_id, access_token, expires)
                          VALUES ($1, $2, $3)
                          ON CONFLICT (user_id) DO UPDATE SET access_token = $2,
                                                              expires      = $3;`;

    //execute the query to get the user
    mySqlConnection.query(getUserQuery, [userID, accessToken, expires], (error) => {

        //pass in the error which may be null and pass the results object which we get the user from if it is not null
        callback(error);
    });
}

function saveRefreshToken(refreshToken, userID, expires, callback) {
    const getUserQuery = `INSERT INTO oauth_refresh_tokens (user_id, refresh_token, expires)
                          VALUES ($1, $2, $3)
                          ON CONFLICT (user_id) DO UPDATE SET refresh_token = $2,
                                                              expires       = $3;`;

    //execute the query to get the user
    mySqlConnection.query(getUserQuery, [userID, refreshToken, expires], (error) => {
            //pass in the error which may be null and pass the results object which we get the user from if it is not null
            callback(error);
        }
    );
}


/**
 * Retrieves the userID from the row which has the spcecified bearerToken. It passes the userID
 * to the callback if it has been retrieved else it passes null
 *
 * @param bearerToken
 * @param callback - takes the user id we if we got the userID or null to represent an error
 */
function getUserIDFromBearerToken(bearerToken, callback) {

    //create query to get the userID from the row which has the bearerToken
    const getUserIDQuery = `SELECT *
                            FROM oauth_access_tokens
                            WHERE access_token = $1;`;

    //execute the query to get the userID
    console.log(mySqlConnection);
    mySqlConnection.query(getUserIDQuery, [bearerToken], (error, results) => {
        //get the userID from the results if its available else assign null
        const userID = results !== null && results.rows.length === 1 ?
            results.rows[0] : null;
        callback(userID);
    });
}

function getUserIDFromRefreshToken(refreshToken, callback) {

    //create query to get the userID from the row which has the bearerToken
    const getUserIDQuery = `SELECT *
                            FROM oauth_refresh_tokens
                            WHERE refresh_token = $1;`;

    //execute the query to get the userID
    const array = [refreshToken];

    mySqlConnection.query(getUserIDQuery, array, (error, results) => {
        //get the userID from the results if its available else assign null
        const userID = results !== null && results.rows.length === 1 ?
            results.rows[0] : null;
        callback(userID);
    });
}